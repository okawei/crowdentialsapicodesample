<?php


    Route::group(['prefix'=>'/iv/v4'], function () {

        Route::group(['before'=>'auth.api.v4'], function(){


            Route::post('/request', 'Controllers\api\v4\RequestController@postRequest');
            Route::delete('/request', 'Controllers\api\v4\RequestController@deleteRequests');
            Route::get('/request', 'Controllers\api\v4\RequestController@getRequest');
            Route::get('/request/{ids}', 'Controllers\api\v4\RequestController@getRequests');
            Route::delete('/request/{ids}', 'Controllers\api\v4\RequestController@deleteRequests');
            Route::post('/request/{id}', 'Controllers\api\v4\RequestController@updateRequest');

            Route::get('/type', 'Controllers\api\v4\TypeController@getType');
            Route::get('/type/{ids}', 'Controllers\api\v4\TypeController@getTypes');

            Route::get('/status', 'Controllers\api\v4\StatusController@getStatus');
            Route::get('/status/{ids}', 'Controllers\api\v4\StatusController@getStatuses');

            Route::get('/investor', 'Controllers\api\v4\InvestorController@getInvestor');
            Route::get('/investorId', 'Controllers\api\v4\InvestorController@getRequestsInvestorId');
            Route::get('/investor/{ids}', 'Controllers\api\v4\InvestorController@getInvestors');
            Route::get('/investorId/{ids}', 'Controllers\api\v4\InvestorController@getRequestsInvestorIds');
            Route::get('/investorId/search/{investorId}', 'Controllers\api\v4\InvestorController@getSpecificRequestsInvestorId');

            Route::get('/verifier', 'Controllers\api\v4\VerifierController@getVerifier');
            Route::get('/verifier/{ids}', 'Controllers\api\v4\VerifierController@getVerifiers');

            Route::get('/certificate', 'Controllers\api\v4\CertificateController@getCertificateJSON');
            Route::get('/certificate/html/{id}','Controllers\api\v4\CertificateController@getCertificateHTML');
            Route::get('/certificate/{ids}', 'Controllers\api\v4\CertificateController@getCertificateJSONs');

            Route::get('/expired', 'Controllers\api\v4\RequestController@getExpired');
            Route::get('/expired/{ids}', 'Controllers\api\v4\RequestController@getExpireds');

            Route::get('/letter','Controllers\api\v4\LetterController@getLetter');
            Route::get('/letter/private/{secureId}', 'Controllers\api\v4\LetterController@getLetterPrivate');
            Route::get('/letter/{ids}','Controllers\api\v4\RequestController@getLetters');

            Route::post('/resend/{ids}','Controllers\api\v4\RequestController@resendInvestor');


            Route::post('/testing/nextStep/{ids}', 'Controllers\api\v4\TestingController@nextStep');
            Route::post('/testing/previousStep/{ids}', 'Controllers\api\v4\TestingController@previousStep');
            Route::delete('/testing/purge', 'Controllers\api\v4\TestingController@purge');
            Route::post('/testing/seed', 'Controllers\api\v4\TestingController@seed');

        });

        Route::get('/link/{publicKey}', 'Controllers\api\v4\RequestController@getIframe');
        Route::get('/letter/{publicKey}/{secureId}', 'Controllers\api\v4\LetterController@getLetterPublic');
    });

    Route::get('ba/v3/iframe', 'Controllers\api\APIController@baIframe');

});
