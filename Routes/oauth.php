<?php
use Guzzle\Http\Client;
App::singleton('oauth2', function() {

    $storage = new OAuth2\Storage\Pdo(array('dsn' => 'mysql:dbname=oauth;host='.Config::get('database.connections.oauth.host'), 'username' => Config::get('database.connections.oauth.username'), 'password' => Config::get('database.connections.oauth.password')));
    $server = new OAuth2\Server($storage);

    $server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

    return $server;
});


Route::post('api/iv/v4/token', function()
{
    $requestInstance = Request::instance();
    $requestInstance->request = Request::json();

    $bridgedRequest  = OAuth2\HttpFoundationBridge\Request::createFromRequest($requestInstance);
    $bridgedResponse = new OAuth2\HttpFoundationBridge\Response();

    $bridgedResponse = App::make('oauth2')->handleTokenRequest($bridgedRequest, $bridgedResponse);

    return $bridgedResponse;
});


Route::group(['before'=>'noProduction'], function(){

    Route::get('/randomtoken', function(){
        $requests = \Models\iv\Request::all();
        $requests = $requests->filter(function($request){
            if($request->apiId != null && $request->verification != null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }

        return $accessToken;
    });


    Route::get('/getValidAccessTokenRequestBody', function(){
        $key = APIKey::orderByRaw('RAND()')->first();
        $result = [
            "grant_type"=>"client_credentials",
            "client_id"=>$key->publicKey,
            "client_secret"=>$key->privateKey
        ];
        return json_encode($result);
    });

    Route::get('/getValidRequestIdForAuthToken', function(){
        $requests = \Models\iv\Request::all();
        $requests = $requests->filter(function($request){
            if($request->apiId != null && $request->verification != null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }

        return json_encode([
            'requestId'=>$request->id,
            'token'=>$accessToken
        ]);
    });

    Route::get('/getExistingInvestorDetails', function(){
        $accessToken = \Models\oauth\AccessToken::latest('expires')->first();
        $key = APIKey::where('publicKey', $accessToken->client_id)->first();
        if($key->type == 1){
            \Config::set('database.connections.mysql', \Config::get('database.connections.iv_sandbox'));
            \DB::reconnect();
        }
        $request = \Models\iv\Request::where('apiId', $key->id)->orderByRaw('RAND()')->first();
        return json_encode([
            "investorEmail"=>$request->investorEmail,
            "token"=>$accessToken->access_token
        ]);
    });

    Route::get('/getExistingVerifierDetails', function(){
        $requests = \Models\iv\Request::all();
        $requests = $requests->filter(function($request){
            if($request->apiId != null && $request->verification != null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        $email = null;
        if($request->report->type == 1){
            $email = $request->getReport()->verifierEmail;
        } else{
            $email = 'jrench@stark-knoll.com';
        }
        return json_encode([
            'verifierEmail'=>$email,
            'token'=>$accessToken
        ]);
    });

    Route::get('/getValidVerifierRequest', function(){
        $verifications = \Models\iv\Verification::orderByRaw('RAND()')->get();
        $request = null;
        foreach($verifications as $verification){
            if($verification->request->apiId != null){
                $request = $verification->request;
                break;
            }
        }

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            "requestId"=>$request->id,
            "token"=> $accessToken
        ]);
    });

    Route::get('/getValidReportRequest', function(){
        $reports = \Models\iv\Report::orderByRaw('RAND()')->get();
        $request = null;
        foreach($reports as $report){
            if($report->request->apiId != null){
                $request = $report->request;
                break;
            }
        }

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            "requestId"=>$request->id,
            "token"=> $accessToken
        ]);
    });

    Route::get('/getValidCertificateRequest', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->apiId != null && $request->verification != null){
                if($request->verification->confirmed){
                    return true;
                }

            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            "requestId"=>$request->id,
            "token"=> $accessToken
        ]);
    });
    Route::get('/getValidInvestorIdRequest', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->investorId != null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            "requestId"=>$request->id,
            "token"=> $accessToken
        ]);
    });
    Route::get('/getValidInvestorId', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->investorId != null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            "investorId"=>$request->investorId->investorId,
            "token"=> $accessToken
        ]);
    });

    Route::get('/getAllRequestIdsDetails', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
            $accessToken = \Models\oauth\AccessToken::where('access_token', $accessToken)->first();
        }
        $key = APIKey::where('publicKey', $accessToken->client_id)->first();
        if($key->type == 1){
            \Config::set('database.connections.mysql', \Config::get('database.connections.iv_sandbox'));
            \DB::reconnect();
        }

        $requests = \Models\iv\Request::where('apiId', $key->id)->get();
        $requestIds = $requests->map(function($request){
            return $request->id;
        });
        $requestIds = $requestIds->toArray();

        return json_encode([
           'requestIds'=>$requestIds,
            'token'=>$accessToken->access_token
        ]);

    });

    Route::get('/getRequestSentToInvestor', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->report == null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestId'=> $request->id,
            'token'=>$accessToken
        ]);
    });

    Route::get('/getRequestThirdParty', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                if($request->getReport()->verificationType == 'thirdParty' && $request->verification->confirmed){
                    return true;
                }
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestId'=> $request->id,
            'token'=>$accessToken
        ]);
    });


    Route::get('/getRequestUpload', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->report != null){
                if($request->getReport()->verificationType == 'upload' || $request->report->type != 1){
                    if($request->verification != null){
                        if($request->verification->confirmed){
                            return true;
                        }
                    }
                }
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestId'=> $request->id,
            'token'=>$accessToken
        ]);
    });

    Route::get('/getRequestThirdPartyAndUpload', function(){
        $keys = APIKey::all();
        $keyWeNeed = $thirdPartyId = $uploadId = null;
        $found = false;
        foreach($keys as $key){
            $keyWeNeed = $key;
            if($found){
                break;
            }
            $validKeyRequests = $key->requests->filter(function($request){
               if($request->verification != null){
                   if($request->verification->confirmed){
                       return true;
                   }
               }

            });

            $thirdPartyId = $uploadId = null;
            foreach($validKeyRequests as $request){
                if($thirdPartyId == null || $uploadId == null){
                    if($request->getReport()->verificationType == 'thirdParty'){
                        $thirdPartyId = $request->id;
                    }else{
                        $uploadId = $request->id;
                    }
                } else{
                    $found = true;
                    break;
                }
            }
        }

        if($thirdPartyId == null || $uploadId == null){
            $testingSeeder = new TestingSeeder();
            $keyWeNeed = $testingSeeder->run();
            $requests = $keyWeNeed->requests->toArray();
            $thirdPartyId = $requests[count($requests)-1]['id'];
            $uploadId = $requests[count($requests)-2]['id'];
        }

        $accessToken = \Models\oauth\AccessToken::where('client_id', $keyWeNeed->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestIds'=> [$thirdPartyId, $uploadId],
            'token'=>$accessToken
        ]);
    });

    Route::get('/getRequestConfirmedAndNotConfirmed', function(){
        $keys = APIKey::all();
        $keyWeNeed = $confirmedId = $notConfirmedId = null;
        $found = false;

        foreach($keys as $key){
            $keyWeNeed = $key;
            if($confirmedId != null && $notConfirmedId != null){
                break;
            }

            $confirmedId = $notConfirmedId = null;
            foreach($key->requests as $request){
                if($confirmedId == null || $notConfirmedId == null){
                    if($request->verification != null){
                        if($request->verification->confirmed){
                            $confirmedId = $request->id;
                        }
                    } else{
                        $notConfirmedId = $request->id;
                    }
                }

                if($confirmedId != null && $notConfirmedId != null){
                    break;
                }

            }
        }

        $accessToken = \Models\oauth\AccessToken::where('client_id', $keyWeNeed->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestIds'=> [$confirmedId, $notConfirmedId],
            'token'=>$accessToken
        ]);
    });
    Route::get('/getUnconfirmedRequest', function(){
        $requests = \Models\iv\Request::where('apiId', '!=', '0')->get();
        $requests = $requests->filter(function($request){
            if($request->report == null){
                return true;
            }
        });
        $request = $requests->get(array_rand($requests->toArray()));

        $accessToken = \Models\oauth\AccessToken::where('client_id', $request->key->publicKey)->first();
        if($accessToken == null){
            $key = APIKey::find($request->apiId);
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'requestId'=> $request->id,
            'token'=>$accessToken
        ]);
    });

    Route::get('/getProductionRequest', function(){
        $key = APIKey::where('type', 2)->first();
        $request = $key->requests->get(array_rand($key->requests->toArray()));


        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }

        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$request->id
        ]);

    });

    Route::get('/getSandboxRequest', function(){
        $key = APIKey::where('type', 1)->first();
        $request = $key->requests->get(array_rand($key->requests->toArray()));


        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }

        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$request != null ? $request->id : null
        ]);

    });

    Route::get('/getCompleteSandboxRequest', function(){
        $keys = APIKey::where('type', 1)->get();
        $requestId = $keyNeeded = null;

        foreach($keys as $key){
            $requests = $key->requests->filter(function($request){
                if($request->verification != null){
                    if($request->verification->confirmed){
                        return true;
                    }
                }
            });
            if($requests->count() != 0){
                $requestId = $requests->first()->id;
                $keyNeeded = $key;
                break;
            }
        }
        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$requestId
        ]);

    });
    Route::get('/getStartedSandboxRequest', function(){
        $keys = APIKey::where('type', 1)->get();
        $requestId = $keyNeeded = null;

        foreach($keys as $key){
            $requests = $key->requests->filter(function($request){
                if($request->report == null){
                    return true;
                }
            });
            if($requests->count() != 0){
                $requestId = $requests->first()->id;
                $keyNeeded = $key;
                break;
            }
        }
        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$requestId
        ]);

    });

    Route::get('/getIncompleteSandboxRequest', function(){
        $keys = APIKey::where('type', 1)->get();
        $requestId = $keyNeeded = null;

        foreach($keys as $key){
            $requests = $key->requests->filter(function($request){
                if($request->getStatus()['code'] != 4){
                    return true;
                }
            });
            if($requests->count() != 0){
                $requestId = $requests->first()->id;
                $keyNeeded = $key;
                break;
            }
        }
        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$requestId
        ]);

    });
    Route::get('/getNotStartedSandboxRequest', function(){
        $keys = APIKey::where('type', 1)->get();
        $requestId = $keyNeeded = null;

        foreach($keys as $key){
            $requests = $key->requests->filter(function($request){
                if($request->getStatus()['code'] != 1){
                    return true;
                }
            });
            if($requests->count() != 0){
                $requestId = $requests->first()->id;
                $keyNeeded = $key;
                break;
            }
        }
        $accessToken = \Models\oauth\AccessToken::where('client_id', $key->publicKey)->first();
        if($accessToken == null){
            $result = [
                "grant_type"=>"client_credentials",
                "client_id"=>$key->publicKey,
                "client_secret"=>$key->privateKey
            ];
            $body = json_encode($result);
            $requestPayload = ['body'=>$body];
            $client = new GuzzleHttp\Client(['base_url'=>'http://crowdentials.dev']);
            $result = $client->post('/api/iv/v4/token', $requestPayload);
            $accessToken = $result->json()['access_token'];
        } else{
            $accessToken = $accessToken->access_token;
        }
        return json_encode([
            'token'=>$accessToken,
            'requestId'=>$requestId
        ]);

    });

    Route::get('/seedDatabase', function(){
        $seeder = new DatabaseSeeder();
        $seeder->run();
    });

    Route::get('randomCredentials', function(){
        $key = APIKey::orderByRaw('RAND()')->first();
        return json_encode([
            'public'=>$key->publicKey,
            'private'=>$key->privateKey,
            'id'=>$key->id,
            'userId'=>$key->userId
        ]);
    });

    Route::get('randomRequestWithCredentials', function(){
        $requests = \Models\iv\Request::where('issuerId', 0)->get();
        $request = $requests->get(array_rand($requests->toArray()));
        return json_encode([
            'public'=>$request->key->publicKey,
            'private'=>$request->key->privateKey,
            'id'=>$request->id
        ]);
    });
});

