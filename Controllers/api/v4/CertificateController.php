<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\Request;
use League\Fractal\Manager;

class CertificateController extends ApiController
{


    public function getCertificateHTML($id){
        if($this->validateId($id) !== true){
            return $this->validateId($id);
        }
        $request = Request::find($id);
        if($request->apiId == null || $request->apiId != $this->getKeyId()){
            return $this->errorForbidden('You are not authorized to access this request.');
        }

        if($request->getStatus()['code'] < 4){
            return $this->errorInvalidStatus('The request with id '.$id.' is not on the correct status.', $request->getStatus()['code'],4);
        }

        return $this->respondWithItem($request, new RequestCertificateHTMLTransformer);

    }

    public function getCertificateJSON(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                if($request->verification->confirmed){
                    return true;
                }
            }
        });
        return $this->respondWithCollection($requests, new RequestCertificateTransformer);
    }

    public function getCertificateJSONs($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                if($request->verification->confirmed){
                    return true;
                }
            }
        });

        return $this->respondWithCollection($requests, new RequestCertificateTransformer);

    }



}