<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\Request;
use League\Fractal\Manager;

class VerifierController extends ApiController
{

    public function getVerifier(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        /**
         * We only want to return data for requests which actually have a verifier
         * Filter results as such
         */
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                return true;
            }
        });

        if(count(\Input::all())){
            list($validParams, $requests) = $this->filterVerifierRequests($requests);
            if(!$validParams){
                return $this->errorWrongArgs('Invalid arguments provided.  Expected name or email.  Received '.implode(', ',array_keys(\Input::all())));
            }
        }
        return $this->respondWithCollection($requests, new RequestVerifierTransformer);
    }

    public function getVerifiers($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        /**
         * We only want to return data for requests which actually have a verifier
         * Filter results as such
         */
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                return true;
            }
        });

        return $this->respondWithCollection($requests, new RequestVerifierTransformer);

    }

}