<?php



namespace Controllers\api\v4;

use Illuminate\Support\Facades\Response;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use Models\iv\Request;


class ApiController extends \BaseController{
    protected $statusCode = 200;
    protected $keyId = null;
    protected $sandbox = false;

    /**
     * Error codes
     */
    const CODE_WRONG_ARGS = 'INVALID-ARGUMENTS';
    const CODE_NOT_FOUND = 'NOT-FOUND';
    const CODE_INTERNAL_ERROR = 'INTERNAL-ERROR';
    const CODE_UNAUTHORIZED = 'ACCESS-DENIED';
    const CODE_FORBIDDEN = 'FORBIDDEN';
    const CODE_EXPIRED = 'EXPIRED-TOKEN';
    const CODE_INVALID_STATUS = 'INVALID-STATUS';
    const CODE_LIMIT_REACHED = 'CALL-LIMIT-REACHED';

    /**
     * Success codes
     */
    const CODE_DELETED = 'DELETED';
    const CODE_UPDATED = 'UPDATED';
    const CODE_POSTED = 'POSTED';

    public function __construct(Manager $fractal){

        $this->fractal = $fractal;

        $bridgedRequest  = \OAuth2\HttpFoundationBridge\Request::createFromRequest(\Request::instance());
        $bridgedResponse = new \OAuth2\HttpFoundationBridge\Response();

        if (\App::make('oauth2')->verifyResourceRequest($bridgedRequest, $bridgedResponse)) {

            $token = \App::make('oauth2')->getAccessTokenData($bridgedRequest);
            $key = \APIKey::where('publicKey', $token['client_id'])->first();
            if($key->type == 1){
                $this->connectToSandbox();
                $this->sandbox = true;
            }
            $this->keyId = $key->id;

        }
        else {
            $this->setStatusCode(500);
        }



    }

    protected function respondWithError($message, $errorCode){
        if($this->statusCode === 200){
            trigger_error('Why would I return an error with a 200 status code? You decide!', E_USER_WARNING);
        }

        return $this->respondWithArray([
            'error'=> [
                'code'=>$errorCode,
                'http_code'=>$this->statusCode,
                'message'=>$message,

            ]
        ]);
    }




    protected function respondWithSuccess($message, $successCode){
        if($this->statusCode !== 200){
            trigger_error('Why would I return a success without a 200 status code? You decide!', E_USER_WARNING);
        }

        return $this->respondWithArray([
            'success'=> [
                'code'=>$successCode,
                'http_code'=>200,
                'message'=>$message,
            ]
        ]);
    }



    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return null
     */
    public function getKeyId()
    {
        return $this->keyId;
    }

    /**
     * @param null $keyId
     */
    public function setKeyId($keyId)
    {
        $this->keyId = $keyId;
    }

    /**
     * @return boolean
     */
    public function isSandbox()
    {
        return $this->sandbox;
    }

    /**
     * @param boolean $sandbox
     */
    public function setSandbox($sandbox)
    {
        $this->sandbox = $sandbox;
    }

    /**
     * @param $requests
     * @return array
     */
    public function filterInvestorRequests($requests)
    {
        $validParams = false;
        if (\Input::get('name') != null) {
            $validParams = true;
            $requests = $requests->filter(function ($request) {
                if (strtolower($request->investorName) == strtolower(\Input::get('name'))) {
                    return true;
                }
            });
        }
        if (\Input::get('phone') != null) {
            $validParams = true;
            $requests = $requests->filter(function ($request) {
                if (strtolower($request->investorPhone) == strtolower(\Input::get('phone'))) {
                    return true;
                }
            });
        }
        if (\Input::get('email') != null) {
            $validParams = true;
            $requests = $requests->filter(function ($request) {
                if (strtolower($request->investorEmail) == strtolower(\Input::get('email'))) {
                    return true;
                }
            });
            return array($validParams, $requests);
        }
        return array($validParams, $requests);
    }

    /**
     * @param $ids
     * @return \Illuminate\Support\Facades\Response|static
     */
    public function filterRequests($ids)
    {
        $requestsRequested = Request::findMany($ids);
        $requestsAvailable = $requestsRequested->filter(function ($request) {
            if ($request->apiId == $this->getKeyId()) {
                return true;
            }
        });
        $requestsAvailableIds = $requestsAvailable->map(function ($request) {
            return $request->id;
        });
        $notAuthorized = array_diff($ids, $requestsAvailableIds->toArray());
        if (count($notAuthorized)) {
            return $this->errorUnauthorized('You are not authorized to access the requests with ids: ' . implode(', ', $notAuthorized));
        }
        return $requestsAvailable;
    }

    /**
     * @param $requests
     * @return array
     */
    public function filterVerifierRequests($requests)
    {
        $validParams = false;
        if (\Input::get('name') != null) {
            $validParams = true;
            $requests = $requests->filter(function ($request) {
                if ($request->verification->signatureName == \Input::get('name')) {
                    return true;
                }
            });
        }
        if (\Input::get('email') != null) {
            $validParams = true;
            $requests = $requests->filter(function ($request) {
                if ($request->report->type == 1) {
                    if ($request->getReport()->verifierEmail == \Input::get('email')) {
                        return true;
                    }
                } else if (\Input::get('email') == 'jrench@stark-knoll.com') {
                    return true;
                }

            });
            return array($validParams, $requests);
        }
        return array($validParams, $requests);
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection|\Illuminate\Support\Facades\Response|null|static
     */
    public function validateRequestBeforeUpdate($id)
    {
        $request = Request::find($id);
        if ($request == null) {
            return $this->errorForbidden('You are not authorized to update the request with id ' . $id);
        }
        if ($request->apiId != $this->getKeyId()) {
            return $this->errorForbidden('You are not authorized to update the request with id ' . $id);
        }

        if ($request->report != null) {
            return $this->errorInvalidStatus('Request with id ' . $id . ' is not on the appropriate stage.', $request->getStatus()['code'], 1);
        }
        return $request;
    }

    /**
     * @param $id
     * @return bool|\Illuminate\Support\Facades\Response
     */
    public function validateId($id)
    {
        $id = filter_var($id, FILTER_VALIDATE_INT, array(
            'options' => array('min_range' => 1)
        ));
        if ($id === false) {
            return $this->errorWrongArgs('You must pass one and only one argument to this endpoint.');
        }
        return true;
    }

    /**
     * @param $ids
     * @param $requestsAvailable
     * @param $expectedStatus
     * @return \Illuminate\Support\Facades\Response
     */
    public function validateRequestStatus($ids, $requestsAvailable, $expectedStatus)
    {
        if(!is_array($expectedStatus)){
            $expectedStatus = [$expectedStatus];
        }
        $validRequests = $requestsAvailable->filter(function ($request) use ($expectedStatus) {

            if (in_array($request->getStatus()['code'], $expectedStatus)) {
                return true;
            }

        });

        $requestsAvailableIds = $validRequests->map(function ($request) {
            return $request->id;
        });

        $notAuthorized = array_diff($ids, $requestsAvailableIds->toArray());
        if (count($notAuthorized)) {
            $notAuthorizedStatuses = [];
            foreach ($notAuthorized as $id) {
                array_push($notAuthorizedStatuses, Request::find($id)->getStatus()['code']);
            }
            $notAuthorizedStatuses = implode(',', $notAuthorizedStatuses);
            return $this->errorInvalidStatus('The following requests were not on the correct status: ' . implode(', ', $notAuthorized), $notAuthorizedStatuses, implode(', ', $expectedStatus));
        }
        return $validRequests;
    }

    protected function respondWithItem($item, $callback){

        $resource = new Item($item, $callback);
        $rootScope = $this->fractal->createData($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    protected function respondWithCollection($collection, $callback){

        $resource = new Collection($collection, $callback);
        $rootScope = $this->fractal->createData($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    protected function respondWithArray(array $array, array $headers = []){

        /**
         * In some cases if filters are passed we have to remove the null
         * values from the result array and reset the array indexes
         */
        if(isset($array['data']) && count(\Input::all())){
            $array['data'] = array_filter($array['data']);
            $array['data']=array_values($array['data']);
        }
        return Response::json($array, $this->statusCode, $headers);
    }


    /**
     * Generates a Response with a 403 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorForbidden($message = 'Forbidden')
    {
        return $this->setStatusCode(403)->respondWithError($message, self::CODE_FORBIDDEN);
    }
    /**
     * Generates a Response with a 500 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorInternalError($message = 'Internal Error')
    {
        return $this->setStatusCode(500)->respondWithError($message, self::CODE_INTERNAL_ERROR);
    }

    /**
     * Generates a Response with a 404 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorNotFound($message = 'Resource Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message, self::CODE_NOT_FOUND);
    }
    /**
     * Generates a Response with a 401 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorUnauthorized($message = 'Unauthorized')
    {
        return $this->setStatusCode(401)->respondWithError($message, self::CODE_UNAUTHORIZED);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorWrongArgs($message = 'Wrong Arguments')
    {
        return $this->setStatusCode(400)->respondWithError($message, self::CODE_WRONG_ARGS);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorExpiredToken($message = 'Expired token')
    {
        return $this->setStatusCode(403)->respondWithError($message, self::CODE_EXPIRED);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorLimitReached($message = 'Rate limit has been reached')
    {
        return $this->setStatusCode(403)->respondWithError($message, self::CODE_LIMIT_REACHED);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function errorInvalidStatus($message = 'Invalid status', $actual, $expected)
    {
        $this->setStatusCode(403);
        $array = [
            'error'=>[
                'code'=>self::CODE_INVALID_STATUS,
                'http_code'=>$this->statusCode,
                'message'=>$message,
                'expectedStatus'=>$expected,
                'actualStatus'=>$actual
            ]
        ];
        return $this->respondWithArray($array);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function successDeleted($message = 'Successfully deleted resource.')
    {
        return $this->setStatusCode(200)->respondWithSuccess($message, self::CODE_DELETED);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function successUpdated($message = 'Successfully updated resource.')
    {
        return $this->setStatusCode(200)->respondWithSuccess($message, self::CODE_UPDATED);
    }
    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @return  Response
     */
    public function successPosted($message = 'Successfully posted resource.')
    {
        return $this->setStatusCode(200)->respondWithSuccess($message, self::CODE_POSTED);
    }

}