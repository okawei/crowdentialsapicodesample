<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\NaturalReport;
use Models\iv\Request;
use League\Fractal\Manager;
use Models\iv\RequestInvestorId;
use Models\oauth\Call;

class TestingController extends ApiController
{
    public function nextStep($ids){
        if(\APIKey::find($this->getKeyId())->type != 1){
            return $this->errorWrongArgs('You can only call this endpoint for sandbox API keys.');
        }

        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        $requests = $this->validateRequestStatus($ids, $requests, [1,2,3]);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        foreach($requests as $request){

            if($request->getStatus()['code'] == 3){
                $request->verification->confirmed = true;
                $request->verification->save();
            }

            if($request->getStatus()['code'] == 2){
                $this->createVerification($request->report);
            }

            if($request->getStatus()['code'] == 1) {
                $this->createReport($request);
            }

        }

        /**
         * The object is not updated so we have to pull the updated requests from the DB
         */
        $requests =  $this->filterRequests($ids);
        $this->setStatusCode(201);
        return $this->respondWithCollection($requests, new RequestTransformer);


    }

    public function previousStep($ids){
        if(\APIKey::find($this->getKeyId())->type != 1){
            return $this->errorWrongArgs('You can only call this endpoint for sandbox API keys.');
        }
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        $requests = $this->validateRequestStatus($ids, $requests, [2,3,4]);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        foreach($requests as $request){

            if($request->getStatus()['code'] == 2){
                $request->getReport()->forceDelete();
                $request->report->forceDelete();
                continue;
            }

            if($request->getStatus()['code'] == 3){
                $request->verification->forceDelete();
                continue;
            }

            if($request->getStatus()['code'] == 4){
                $request->verification->confirmed = false;
                $request->verification->save();
                continue;
            }
        }
        /**
         * The object is not updated so we have to pull the updated requests from the DB
         */
        $requests =  $this->filterRequests($ids);
        $this->setStatusCode(201);
        return $this->respondWithCollection($requests, new RequestTransformer);


    }

    public function purge(){
        if(\APIKey::find($this->getKeyId())->type != 1){
            return $this->errorWrongArgs('You can only call this endpoint for sandbox API keys.');
        }

        /**
         * This should probably be moved to the queue eventually
         */
        $requests = \APIKey::find($this->getKeyId())->requests;
        foreach($requests as $request){
            $request->delete();
        }

        return $this->successDeleted('Successfully purged sandbox data.');
    }

    public function seed(){
        if(\APIKey::find($this->getKeyId())->type != 1){
            return $this->errorWrongArgs('You can only call this endpoint for sandbox API keys.');
        }

        $count = 10;
        if(\Input::has('count')){
            $count = (int)\Input::get('count');
            if($count > 10){
                return $this->errorWrongArgs('You may only seed up to 10 requests at a time');
            }
        }

        for($x = 0; $x < $count; $x++){
            $request = $this->createRequest();
            if(rand(0,5) > 1){
                $report = $this->createReport($request);
                if(rand(0,5) > 1){
                    $verification = $this->createVerification($report);
                    if(rand(0,1)){
                        $verification->confirmed = true;
                        $verification->save();
                    }
                }
            }
        }
        $call = Call::where('method', 'POST')->where('uri', '/api/iv/v4/testing/seed')->where('keyId', $this->getKeyId())->first();
        $call->count = $call->count+($count-1);
        $call->save();
        return $this->successPosted('Successfully seeded '.$count.' requests.');

    }

    public function createRequest(){

        $faker = \Faker\Factory::create();
        $key = \APIKey::find($this->getKeyId());
        $request = Request::create([
            'apiId'=>$this->getKeyId(),
            'secureId'=>str_random(64),
            'roundName'=>$faker->word,
            'investmentAmount'=>$faker->numberBetween(0, 1000000),
            'previousInvestment'=>$faker->randomElement(['Yes', 'No']),
            'doubts'=>$faker->randomElement(['Yes', 'No']),
            'researchedBackground'=>$faker->randomElement(['Yes', 'No']),
            'howTheyMet'=>$faker->numberBetween(1, 5),
            'investorName'=>$faker->name,
            'investorPhone'=>$faker->phoneNumber,
            'investorEmail'=>$faker->email,
            'created_at'=>$faker->dateTimeBetween($startDate = $key->created_at, $endDate = 'now')
        ]);
        if(rand(0,1)){
            RequestInvestorId::create([
                'requestId'=>$request->id,
                'investorId'=>$faker->word
            ]);
        }

        return $request;
    }


    public function createVerification($report){
        $faker = \Faker\Factory::create();
        $createArray = [
            'requestId' => $report->requestId,
            'secureId' => str_random(64),
            'confirmed' => rand(0, 1),
            'entityName' => 'Stark & Knoll Co., L.P.A.',
            'stateOfIncorporation' => 'OH',
            'zip' => '44333',
            'phoneNumber' => '(330) 572-1315',
            'signatureName' => 'Jim Rench',
            'licenseNumber' => '0020436'

        ];
        $document = true;
        switch ($report->type) {
            case 1:
                $createArray['income'] = rand(0, 1);
                $createArray['netWorth'] = rand(0, 1);
                $createArray['assets'] = rand(0, 1);
                if ($createArray['income'] == 0 && $createArray['netWorth'] == 0 && $createArray['assets'] == 0) {
                    $createArray['aiNone'] = 1;
                }
                $createArray['entityName'] = $faker->company;
                $createArray['stateOfIncorporation'] = $faker->locale;
                $createArray['zip'] = $faker->postcode;
                $createArray['phoneNumber'] = $faker->phoneNumber;
                $createArray['signatureName'] = $faker->name;
                if ($report->getReport()->verifierType == 'CPA' || $report->getReport()->verifierType == 'attorney') {
                    $createArray['licenseNumber'] = $faker->word;
                } else {
                    $createArray['secOrCRD'] = $faker->word;
                    $createArray['licenseNumber'] = '';
                }
                if($report->getReport()->verificationType != 'upload'){
                    $document = false;
                }
                break;
            case 2:
                $createArray['trust'] = rand(0, 1);
                if ($createArray['trust'] == 0) {
                    $createArray['aiNone'] = 1;
                }
                break;
            case 3:
                $createArray['corporation'] = rand(0, 1);
                if ($createArray['corporation'] == 0) {
                    $createArray['aiNone'] = 1;
                }
                break;
            case 4:
                $createArray['llc'] = rand(0, 1);
                if ($createArray['llc'] == 0) {
                    $createArray['aiNone'] = 1;
                }
                break;
            case 5:
                $createArray['partnership'] = rand(0, 1);
                if ($createArray['partnership'] == 0) {
                    $createArray['aiNone'] = 1;
                }
                break;
        }
        $verification = \Models\iv\Verification::create($createArray);
        if($document){
            \Models\iv\Document::create([
                'reportId'=>$report->id,
                'mime'=>'application/pdf',
                'key'=>'sampleLetter',
                'type'=>5
            ]);
        }
        return $verification;
    }


    /**
     * @param $request
     */
    public function createReport($request)
    {
        $faker = \Faker\Factory::create();

        $rand = rand(0, 4);
        switch ($rand) {
            case 0:
                $thirdParty = $faker->randomElement(['Yes', 'No']);
                $outstandingBusiness = $faker->randomElement(['0', '1', '2']);
                $verificationType = $faker->randomElement(['thirdParty', 'upload']);
                $verifierType = $faker->randomElement(['CPA', 'broker/dealer', 'investment adviser', 'attorney']);
                $report = \Models\iv\NaturalReport::create([
                    'requestId' => $request->id,
                    'secureId' => str_random(64),
                    'thirdParty' => $thirdParty,
                    'thirdPartyType' => $thirdParty == 'Yes' ? $faker->sentence(rand(1, 4)) : '',
                    'ownAccount' => '1',
                    'outstandingBusiness' => $outstandingBusiness,
                    'outstandingBusinessAmount' => $outstandingBusiness == '1' ? rand(1000, 1000000) : '',
                    'privatePlacements' => $faker->randomElement(['0', '1-3', '4-6', '7-10', '10+']),
                    'acaMember' => $faker->randomElement(['Yes', 'No']),
                    'selfCertifyAI' => '1',
                    'verificationType' => $verificationType,
                    'hasThirdParty' => $verificationType == 'thirdParty' ? 'true' : 'false',
                    'verifyBasedOn' => $faker->randomElement(['income', 'netWorth', 'both']),
                    'verifierType' => $verificationType == 'thirdParty' ? $verifierType : '',
                    'verifierName' => $verificationType == 'thirdParty' ? $faker->name : 'Jim Rench',
                    'verifierEmail' => $verificationType == 'thirdParty' ? $faker->email : 'jrench@stark-knoll.com',
                    'verifierPhone' => $verificationType == 'thirdParty' ? $faker->phoneNumber : '(330) 572-1315',
                    'investorName' => $verifierType == 'CPA' ? $faker->name : '',
                    'taxReturnYears' => $verificationType == 'CPA' ? $faker->year : '',
                    'consentEffectiveFor' => $verificationType == 'CPA' ? rand(0, 10) . ' years' : '',
                    'signatureName' => $faker->name
                ]);
                if ($verificationType == 'upload') {
                    \Models\iv\NaturalUpload::create([
                        'reportId' => $report->id,
                        'incomeType' => $faker->randomElement([1, 2]),
                        'liabilitiesAmount' => rand(0, 100000)
                    ]);
                }

                $report =  \Models\iv\Report::create([
                    'requestId' => $request->id,
                    'reportId' => $report->id,
                    'type' => 1
                ]);
                break;
            case 1:
                $report = \Models\iv\TrustReport::create([
                    'requestId' => $request->id,
                    'secureId' => str_random(64),
                    'authorizedToAct' => 1,
                    'notFormedForAcquiring' => 1,
                    'sophisticatedPerson' => 1,
                    'tosPPCheck' => 1
                ]);
                $report =  \Models\iv\Report::create([
                    'requestId' => $request->id,
                    'type' => 2,
                    'reportId' => $report->id
                ]);
                break;
            case 2:
                $report = \Models\iv\CorporationReport::create([
                    'requestId' => $request->id,
                    'secureId' => str_random(64),
                    'authorizedToAct' => 1,
                    'notFormedForAcquiring' => 1,
                    'entityName' => $faker->company,
                    'stateOfIncorporation' => $faker->locale,
                    'tosPPCheck' => 1
                ]);
                $report =  \Models\iv\Report::create([
                    'requestId' => $request->id,
                    'reportId' => $report->id,
                    'type' => 3
                ]);
                break;
            case 3:
                $report = \Models\iv\LLCReport::create([
                    'requestId' => $request->id,
                    'secureId' => str_random(64),
                    'authorizedToAct' => 1,
                    'notFormedForAcquiring' => 1,
                    'entityName' => $faker->company,
                    'stateOfIncorporation' => $faker->locale,
                    'tosPPCheck' => 1
                ]);
                $report =  \Models\iv\Report::create([
                    'requestId' => $request->id,
                    'reportId' => $report->id,
                    'type' => 4
                ]);
                break;
            case 4:
                $report = \Models\iv\PartnershipReport::create([
                    'requestId' => $request->id,
                    'secureId' => str_random(64),
                    'authorizedToAct' => 1,
                    'notFormedForAcquiring' => 1,
                    'tosPPCheck' => 1
                ]);
                $report =  \Models\iv\Report::create([
                    'requestId' => $request->id,
                    'type' => 5,
                    'reportId' => $report->id
                ]);
                break;
            default:

        }
        return $report;
    }


}