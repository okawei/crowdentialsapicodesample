<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\Request;
use League\Fractal\Manager;

class InvestorController extends ApiController
{

    public function getInvestor(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        if(count(\Input::all())){
            list($validParams, $requests) = $this->filterInvestorRequests($requests);
            if(!$validParams){
                return $this->errorWrongArgs('Invalid arguments provided.  Expected name, email, or phone.  Received '.implode(', ',array_keys(\Input::all())));
            }
        }

        return $this->respondWithCollection($requests, new RequestInvestorTransformer);
    }

    public function getInvestors($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }
        return $this->respondWithCollection($requests, new RequestInvestorTransformer);

    }

    public function getRequestsInvestorId(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        $requests = $requests->filter(function($request){
            if($request->investorId != null){
                return true;
            }
        });
        return $this->respondWithCollection($requests, new RequestInvestorIdTransformer);
    }

    public function getRequestsInvestorIds($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }
        $requests = $requests->filter(function($request){
            if($request->investorId != null){
                return true;
            }
        });
        return $this->respondWithCollection($requests, new RequestInvestorIdTransformer);
    }

    public function getSpecificRequestsInvestorId($investorId){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        $requests = $requests->filter(function($request) use ($investorId){
            if($request->investorId != null){
                if($request->investorId->investorId == $investorId){
                    return true;
                }
            }
        });
        return $this->respondWithCollection($requests, new RequestTransformer);
    }

}