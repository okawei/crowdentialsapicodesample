<?php

namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Controllers\iv\investor\InitializationController;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Request;
use League\Fractal\Manager;
use Models\iv\RequestInvestorId;

class RequestController extends ApiController{

    public function getIframe($publicKey){
        $initializationController = new InitializationController();
        return $initializationController->getAPI($publicKey);
    }

    public function getRequest(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        return $this->respondWithCollection($requests, new RequestTransformer);
    }

    public function getRequests($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }
        return $this->respondWithCollection($requests, new RequestTransformer);

    }



    public function deleteRequests($ids = null){
        if($ids == null){
            return $this->errorWrongArgs('You must specify an id or a list of ids to delete.');
        }
        $ids = explode(',', $ids);
        $requests = $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        foreach($requests as $request){
            $request->delete();
        }

        return $this->successDeleted('Successfully deleted requests with the following ids: '.implode(', ', $ids));
    }

    public function updateRequest($id){
        if($this->validateId($id) !== true){
            return $this->validateId($id);
        }

        $request = $this->validateRequestBeforeUpdate($id);
        if(get_class($request) != 'Models\iv\Request'){
            return $request;
        }
        $content = (array)json_decode(\Request::getContent());
        if($content == null){
            return $this->errorWrongArgs('Accepted arguments: name, email, phone, investorType');
        }

        if(count(array_diff(array_keys($content), ['name', 'email', 'phone', 'investorType', 'investorId']))){
            return $this->errorWrongArgs('Accepted arguments: name, email, phone, investorType, investorId. Received arguments: ' . implode(', ', array_keys($content)));
        }

        if(isset($content['name'])){
            $request->investorName = $content['name'];
        }
        if(isset($content['email'])){
            if(!filter_var($content['email'], FILTER_VALIDATE_EMAIL)){
                return $this->errorWrongArgs('Invalid email address');
            }
            $request->investorEmail = $content['email'];
        }

        if(isset($content['phone'])){
            $request->investorPhone = $content['phone'];
        }


        if(isset($content['investorType'])){
            if(in_array($content['investorType'], ["natural","trust","corporation","llc","partnership"])){
                $request->investorType = $content['investorType'];
            } else{
                return $this->errorWrongArgs('Invalid investor type.  Expected one of ' . implode(', ', ["natural","trust","corporation","llc","partnership"]).'. Received '.$content['investorType']);
            }

        }

        if(isset($content['investorId'])){


            $investorId = RequestInvestorId::firstOrCreate([
                'requestId'=>$request->id,
            ]);
            $investorId->investorId = $content['investorId'];
            $investorId->save();
        }

        $request->save();
        return $this->successUpdated('Successfully updated the '.implode(array_keys($content)).' fields on request with id '.$id);



    }

    public function getExpired(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                if($request->verification->confirmed){
                    return true;
                }
            }
        });

        return $this->respondWithCollection($requests, new RequestExpiredTransformer);

    }

    public function getExpireds($ids){
        $ids = explode(',', $ids);
        $requestsAvailable =  $this->filterRequests($ids);
        if(get_class($requestsAvailable) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requestsAvailable;
        }

        $requests = $this->validateRequestStatus($ids, $requestsAvailable, 4);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }

        return $this->respondWithCollection($requests, new RequestExpiredTransformer);


    }


    public function getLetters($ids){
        $ids = explode(',', $ids);
        $requestsAvailable =  $this->filterRequests($ids);
        if(get_class($requestsAvailable) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requestsAvailable;
        }

        $requestsAvailable = $this->validateRequestStatus($ids, $requestsAvailable, 4);
        if(get_class($requestsAvailable) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requestsAvailable;
        }

        $validRequests = $requestsAvailable->filter(function($request){
            if($request->report->type != 1 || $request->getReport()->verificationType == 'upload'){
                return true;
            }
        });

        if($validRequests->count() < count($ids)){
            $validIds = $validRequests->map(function($request){
                return $request->id;
            });

            $invalidIds = array_diff($ids,$validIds->toArray());

            return $this->errorWrongArgs('Requests with the following ids are not verified via document upload so no letter is present: '.implode(', ', $invalidIds));
        }

        return $this->respondWithCollection($validRequests, new RequestLetterTransformer);

    }

    public function resendInvestor($ids){
        $ids = explode(',', $ids);
        $requestsAvailable =  $this->filterRequests($ids);
        if(get_class($requestsAvailable) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requestsAvailable;
        }

        $requestsAvailable = $this->validateRequestStatus($ids, $requestsAvailable, 1);
        if(get_class($requestsAvailable) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requestsAvailable;
        }

        foreach($requestsAvailable as $request){
            if(!filter_var($request->investorEmail, FILTER_VALIDATE_EMAIL)){
                return $this->errorWrongArgs('Invalid email address');
            }
            $request->touch();
            \Event::fire('iv.newRequest', $request);
        }

        return $this->successPosted('Successfully resent requests with ids: '.implode(', ', $ids));

    }

    public function postRequest(){
        $content = (array)json_decode(\Request::getContent());
        if($content == null){
            return $this->errorWrongArgs('Accepted arguments: name, email, phone.  Optional arguments: investorType, investorId');
        }

        if(!array_key_exists('name', $content) || !array_key_exists('email', $content) || !array_key_exists('phone', $content)){
            return $this->errorWrongArgs('Accepted arguments: name, email, phone.  Received arguments: '.implode(', ', array_keys($content)));
        }

        $validator = \Validator::make($content,
            [
                'name'=>'required',
                'email'=>'required|email',
                'phone'=>'required',
                'investorType'=>'in:natural,trust,corporation,llc,partnership'
            ]);
        if($validator->fails()){
            return $this->errorWrongArgs('Errors validating request body: '. implode(' ', $validator->messages()->all()));
        }

        $request = Request::create([
            'issuerId'=>0,
            'apiId'=>$this->getKeyId(),
            'secureId'=>str_random(64),
            'investorName'=>$content['name'],
            'investorEmail'=>$content['email'],
            'investorPhone'=>$content['phone'],
            'investorType'=>isset($content['investorType']) ? $content['investorType'] : "",
        ]);
        if(isset($content['investorId'])){
            RequestInvestorId::create([
                'requestId'=>$request->id,
                'investorId'=>$content['investorId']
            ]);
        }

        \Event::fire('iv.newRequest', $request);

        /**
         * Add the amount owed to the user making the requests' account
         */
        if($request->key->type == 2 && $request->key->user->details->sass && $request->key->user->details->activated){
            $request->key->user->apiBilling->amountOwed = $request->key->user->apiBilling->amountOwed+$request->key->user->apiBilling->transactionAmount;
            $request->key->user->apiBilling->save();
        }

        $this->setStatusCode(201);
        return $this->respondWithArray([
            'success'=>[
                'code'=>self::CODE_POSTED,
                'http_code'=>201,
                'message'=>'Successfully created request.',
                'id'=>$request->id

            ]
        ]);

    }


}