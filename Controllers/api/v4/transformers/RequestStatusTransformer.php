<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestStatusTransformer extends TransformerAbstract{


    public function transform(Request $request){
        $status = $request->getStatus();
        if($status['code'] == 4){
            if($request->verification->aiNone || $request->doubts == 'Yes'){
                $status['status']='Unaccredited';
                $status['details']='The verifier stated that this investor does not meet the minimum thresholds for being accredited or the issuer stated they have reason to doubt this investors accredited status.';
            }
        }

        return [
            'id'=>(int)$request->id,
            'status'=>(string)$status['status'],
            'statusCode'=>(int)$status['code'],
            'statusDetails'=>(string)$status['details']
        ];



    }
}