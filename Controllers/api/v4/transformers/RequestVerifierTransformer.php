<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestVerifierTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $type = null;
        $updatedAt = $request->updated_at;
        $individualName = $entityName = $email = $phone = $zip = $state = $type = $secOrCRD = $licenseNumber = null;
        if($request->verification != null){
            $individualName = $request->verification->signatureName;
            $entityName = $request->verification->entityName;
            $email = $request->report->type == 1 ? $request->getReport()->verifierEmail : 'jrench@stark-knoll.com';
            $phone = $request->verification->phoneNumber;
            $zip = $request->verification->zip;
            $state = $request->verification->stateOfIncorporation;
            $type = $request->report->type == 1 ? $request->getReport()->verifierType : 'attorney';
            $secOrCRD = $request->verification->secOrCRD;
            $licenseNumber = $request->verification->licenseNumber;
            $updatedAt = $request->verification->created_at;
        }

        return [
            'id'=>(int)$request->id,
            'individualName'=>$individualName,
            'entityName'=>$entityName,
            'email'=>$email,
            'phone'=>$phone,
            'zip'=>$zip,
            'state'=>$state,
            'type'=>$type,
            'secOrCRD'=>$secOrCRD,
            'licenseNumber'=>$licenseNumber,
            'createdAt'=>(string)$request->created_at,
            'updatedAt'=>(string)$updatedAt,
        ];
    }
}