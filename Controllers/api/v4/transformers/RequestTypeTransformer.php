<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestTypeTransformer extends TransformerAbstract{


    public function transform(Request $request){
        $type = null;
        $typeCode = null;

        if($request->report != null){
            $type = $request->report->getTypeText();
            $typeCode = $request->report->type;
        }

        return [
            "id" => (int)$request->id,
            "typeCode" => (int)$typeCode,
            "typeDetails" => (string)$type
        ];

    }
}