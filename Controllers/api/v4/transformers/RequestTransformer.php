<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $investor = new RequestInvestorTransformer();
        $investor = $investor->transform($request);
        unset($investor['id']);
        $status = new RequestStatusTransformer();
        $status = $status->transform($request);
        unset($status['id']);
        $verifier = null;
        if($request->verification != null){
            $verifier = new RequestVerifierTransformer();
            $verifier = $verifier->transform($request);
            unset($verifier['id']);
        }

        return [
            'id'=>(int)$request->id,
            'investor'=>(array)$investor,
            'status'=>(array)$status,
            'verifier'=>$verifier
        ];
    }
}