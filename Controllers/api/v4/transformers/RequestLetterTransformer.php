<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestLetterTransformer extends TransformerAbstract{


    public function transform(Request $request){
        return [
            'id'=>(int)$request->id,
            'publicURL'=>(string)\URL::to('/api/iv/v4/letter/'.$request->key->publicKey.'/'.$request->secureId),
            'privateURL'=>(string)\URL::to('/api/iv/v4/letter/private/'.$request->secureId)
        ];
    }
}