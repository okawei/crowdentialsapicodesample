<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestInvestorTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $type = $request->investorType;
        $updatedAt = $request->updated_at;

        if($request->report != null){
            $type = $request->report->getTypeText();
            $updatedAt = $request->report->updated_at;
        }
        $type = strtolower($type);
        if($type == 'natural person'){
            $type = 'natural';
        }




        return [
            'id'=>(int)$request->id,
            'name'=>(string)$request->investorName,
            'email'=>(string)$request->investorEmail,
            'phone'=>(string)$request->investorPhone,
            'investorId'=>$request->investorId != null ? $request->investorId->investorId : null,
            'type'=>$type != null ? (string)$type : null,
            'createdAt'=>(string)$request->created_at,
            'updatedAt'=>(string)$updatedAt,
        ];
    }
}