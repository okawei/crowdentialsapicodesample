<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestCertificateHTMLTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $cert = new \Certificate($request);
        $html = $cert->getHTML();
        $html = trim(preg_replace('/\s\s+/', ' ', $html));
        return [
            'html'=>$html
        ];
    }
}