<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestInvestorIdTransformer extends TransformerAbstract{
    public function transform(Request $request){
        return [
            'id'=>(int)$request->id,
            'investorId'=>(string)$request->investorId->investorId
        ];
    }
}