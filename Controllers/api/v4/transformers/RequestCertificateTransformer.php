<?php

namespace Controllers\api\v4\transformers;

use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestCertificateTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $cert = new \Certificate($request);
        $responseArray = [];
        $responseArray['id']=$request->id;
        $responseArray = array_merge($responseArray, $cert->getArray());
        unset($responseArray['verifierInformation']);
        unset($responseArray['verifierFName']);
        unset($responseArray['verifierLName']);
        $verifierData = new RequestVerifierTransformer();
        $verifierData = $verifierData->transform($request);
        unset($verifierData['id']);
        $responseArray['verifier']=['data'=>$verifierData];
        return $responseArray;
    }
}