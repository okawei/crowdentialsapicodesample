<?php

namespace Controllers\api\v4\transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Models\iv\Request;

class RequestExpiredTransformer extends TransformerAbstract{
    public function transform(Request $request){
        $verificationDate = $request->verification->created_at;
        $diff = Carbon::now()->diffInDays($verificationDate);
        $expired = false;
        if($diff > 90){
            $expired = true;
        }
        $investorData = new RequestInvestorTransformer();
        $investorData = $investorData->transform($request);
        unset($investorData['id']);
        return [
            'id'=>(int)$request->id,
            'expired'=>(bool)$expired,
            'investor'=>['data'=>$investorData]
        ];
    }
}