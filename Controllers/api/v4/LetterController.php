<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\Request;
use League\Fractal\Manager;

class LetterController extends ApiController
{

    public function getLetterPrivate($secureId){
        $request = Request::where('apiId', $this->getKeyId())->where('secureId', $secureId)->first();
        $key = \APIKey::find($this->getKeyId());
        $bucket = 'crowdentials';
        if($key->type == 1){
            $this->connectToSandbox();
            $bucket = 'crowdentialssandbox';
        }
        $request = $this->validateRequest($request);
        if(get_class($request) != 'Models\iv\Request'){
            return $request;
        }
        echo $this->getDocument($request, $bucket, $key);
    }

    public function getLetterPublic($publicKey, $secureId){
        $key = \APIKey::where('publicKey', $publicKey)->first();
        if($key == null){
            return $this->errorNotFound('Key was not found.');
        }
        $bucket = 'crowdentials';
        if($key->type == 1){
            $this->connectToSandbox();
            $bucket = 'crowdentialssandbox';
        }

        $request = Request::where('apiId', $key->id)->where('secureId', $secureId)->first();
        $request = $this->validateRequest($request);
        if(get_class($request) != 'Models\iv\Request'){
            return $request;
        }
        echo $this->getDocument($request, $bucket, $key);

    }


    public function getLetter(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        $requests = $requests->filter(function($request){
            if($request->verification != null){
                if($request->verification->confirmed){
                    if($request->report->type != 1 || $request->getReport()->verificationType == 'upload'){
                        return true;
                    }
                }
            }
        });
        return $this->respondWithCollection($requests, new RequestLetterTransformer);
    }


    public function getDocument($request, $bucket, $key){
        $document = Document::where('reportId', $request->report->id)->where('type', 5)->first();
        $s3 = \AWS::get('s3');
        $data = $s3->getObject(array(
            'Bucket'	=>$bucket,
            'Key'		=>$document->key
        ))['Body'];

        $decrypted = $data;
        if($key->type == 2){
            $decrypted = \Crypt::decrypt($data);
        }

        header('Content-Type: '.$document->mime);
        echo $decrypted;
    }

    /**
     * @param $request
     * @return \Illuminate\Support\Facades\Response
     */
    public function validateRequest($request)
    {
        if ($request == null) {
            return $this->errorNotFound('Request was not found.');
        }

        if ($request->verification == null) {
            return $this->errorInvalidStatus('Request was on the wrong status.', $request->getStatus()['code'], 4);
        } elseif (!$request->verification->confirmed) {
            return $this->errorInvalidStatus('Request was on the wrong status.', $request->getStatus()['code'], 4);
        }

        if ($request->getReport()->verificationType != 'upload' && $request->report->type == 1) {
            return $this->errorWrongArgs('Request was verified via third party, therefore no letter exists.');
        }
        return $request;
    }
}