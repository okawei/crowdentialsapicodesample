<?php
namespace Controllers\api\v4;


use Carbon\Carbon;
use Controllers\api\v4\transformers\RequestCertificateHTMLTransformer;
use Controllers\api\v4\transformers\RequestCertificateTransformer;
use Controllers\api\v4\transformers\RequestExpiredTransformer;
use Controllers\api\v4\transformers\RequestInvestorIdTransformer;
use Controllers\api\v4\transformers\RequestInvestorTransformer;
use Controllers\api\v4\transformers\RequestLetterTransformer;
use Controllers\api\v4\transformers\RequestStatusTransformer;
use Controllers\api\v4\transformers\RequestTransformer;
use Controllers\api\v4\transformers\RequestTypeTransformer;
use Controllers\api\v4\transformers\RequestVerifierTransformer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use Models\iv\Document;
use Models\iv\Request;
use League\Fractal\Manager;

class StatusController extends ApiController
{

    public function getStatus(){
        $requests = Request::where('apiId', $this->getKeyId())->get();
        if(count(\Input::all())){
            if(\Input::get('code') != null){
                $codes = explode(',', \Input::get('code'));
                $requests = $requests->filter(function($request) use ($codes){
                    if(in_array($request->getStatus()['code'], $codes)){
                        return true;
                    }
                });
            } else{
                return $this->errorWrongArgs('Invalid arguments provided.  Expected code.  Received '.implode(', ',array_keys(\Input::all())));
            }
        }
        return $this->respondWithCollection($requests, new RequestStatusTransformer);
    }

    public function getStatuses($ids){
        $ids = explode(',', $ids);
        $requests =  $this->filterRequests($ids);
        if(get_class($requests) !== 'Illuminate\Database\Eloquent\Collection'){
            return $requests;
        }
        return $this->respondWithCollection($requests, new RequestStatusTransformer);

    }

}