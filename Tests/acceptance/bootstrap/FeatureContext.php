<?php

use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Event\SuiteEvent;
use Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

require_once __DIR__.'/../../../../vendor/phpunit/phpunit/PHPUnit/Autoload.php';
require_once __DIR__.'/../../../../vendor/phpunit/phpunit/PHPUnit/Framework/Assert/Functions.php';

/**
 * Features context.
 */
class FeatureContext extends BehatContext
{
    /**
     * The Guzzle HTTP Client.
     */
    protected $client;

    /**
     * The current resource
     */
    protected $resource;

    /**
     * The request payload
     */
    protected $requestPayload;

    /**
     * The Guzzle HTTP Response.
     */
    protected $response;

    /**
     * The decoded response object.
     */
    protected $responsePayload;

    /**
     * The current scope within the response payload
     * which conditions are asserted against.
     */
    protected $scope;

    /**
     * Access code generated for each test
     */
    protected $accessToken;

    protected $appURL = 'http://xcrowdentials.dev/';
    /**
     * Initializes context.
     * Every scenario gets it's own context object.
     *
     * @param array $parameters context parameters (set them up through behat.yml)
     */
    public function __construct(array $parameters)
    {
        $config = isset($parameters['guzzle']) && is_array($parameters['guzzle']) ? $parameters['guzzle'] : [];

        $config['base_url'] = $parameters['base_url'];

        $this->client = new Client($config);
        $this->requestPayload = [
            "body"=>[],
            "headers"=>[]
        ];
    }

    /**
     * @Given /^I have the payload:$/
     */
    public function iHaveThePayload(PyStringNode $requestPayload)
    {
        $this->requestPayload["body"] = (string)$requestPayload;
    }

    /**
     * @Given /^I have the headers:$/
     */
    public function iHaveTheHeaders(PyStringNode $headers)
    {
        $this->requestPayload["headers"] = (array)json_decode($headers);
    }


    /**
     * @When /^I request "(GET|PUT|POST|DELETE) ([^"]*)" "(with|without)" authorization$/
     */
    public function iRequest($httpMethod, $resource, $authorization)
    {

        if($authorization == 'with'){
            $accessToken = file_get_contents($this->appURL.'randomtoken');
            $this->requestPayload['headers']=["Authorization"=>"Bearer ".$accessToken];
        } else{
            $this->requestPayload['headers']=["Authorization"=>"This is not a real token"];
        }
        $this->resource = $resource;
        $method = strtolower($httpMethod);

        try {
            switch ($httpMethod) {
                case 'PUT':
                case 'POST':
                    $this->response = $this
                        ->client
                        ->$method($resource, $this->requestPayload);
                    break;

                default:
                    $this->response = $this
                        ->client
                        ->$method($resource, $this->requestPayload);
            }
        } catch (BadResponseException $e) {

            $response = $e->getResponse();

            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();

        }
    }

    /**
     * @Then /^I get a "([^"]*)" response$/
     */
    public function iGetAResponse($statusCode)
    {
        $response = $this->getResponse();
        $contentType = $response->getHeader('Content-Type');

        if ($contentType === 'application/json') {
            $bodyOutput = $response->getBody();
        } else {
            $bodyOutput = 'Output is '.$contentType.', which is not JSON and is therefore scary. Run the request manually.';
        }
        assertSame((int) $statusCode, (int) $this->getResponse()->getStatusCode(), $bodyOutput);
    }

    /**
     * @Given /^the "([^"]*)" property equals "([^"]*)"$/
     */
    public function thePropertyEquals($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        assertEquals(
            $actualValue,
            $expectedValue,
            "Asserting the [$property] property in current scope equals [$expectedValue]: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property exists$/
     */
    public function thePropertyExists($property)
    {
        $payload = $this->getScopePayload();

        $message = sprintf(
            'Asserting the [%s] property exists in the scope [%s]: %s',
            $property,
            $this->scope,
            json_encode($payload)
        );

        if (is_object($payload)) {
            assertTrue(array_key_exists($property, get_object_vars($payload)), $message);

        } else {
            assertTrue(array_key_exists($property, $payload), $message);
        }
    }

    /**
     * @Given /^the "([^"]*)" property is an array$/
     */
    public function thePropertyIsAnArray($property)
    {
        $payload = $this->getScopePayload();

        $actualValue = $this->arrayGet($payload, $property);

        assertTrue(
            is_array($actualValue),
            "Asserting the [$property] property in current scope [{$this->scope}] is an array: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is an object$/
     */
    public function thePropertyIsAnObject($property)
    {
        $payload = $this->getScopePayload();

        $actualValue = $this->arrayGet($payload, $property);

        assertTrue(
            is_object($actualValue),
            "Asserting the [$property] property in current scope [{$this->scope}] is an object: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is an empty array$/
     */
    public function thePropertyIsAnEmptyArray($property)
    {
        $payload = $this->getScopePayload();
        $scopePayload = $this->arrayGet($payload, $property);

        assertTrue(
            is_array($scopePayload) and $scopePayload === [],
            "Asserting the [$property] property in current scope [{$this->scope}] is an empty array: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property contains (\d+) items$/
     */
    public function thePropertyContainsItems($property, $count)
    {
        $payload = $this->getScopePayload();

        assertCount(
            $count,
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property contains [$count] items: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property contains at least (\d+) items$/
     */
    public function thePropertyContainsAtLeastItems($property, $count)
    {
        $payload = $this->getScopePayload();
        assertGreaterThanOrEqual(
            $count,
            count($this->arrayGet($payload, $property)),
            "Asserting the [$property] property contains more than [$count] items: ".json_encode($payload));
    }


    /**
     * @Given /^the "([^"]*)" property is an integer$/
     */
    public function thePropertyIsAnInteger($property)
    {
        $payload = $this->getScopePayload();

        isType(
            'int',
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property in current scope [{$this->scope}] is an integer: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a string$/
     */
    public function thePropertyIsAString($property)
    {
        $payload = $this->getScopePayload();

        isType(
            'string',
            $this->arrayGet($payload, $property),
            "Asserting the [$property] property in current scope [{$this->scope}] is a string: ".json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a string equalling "([^"]*)"$/
     */
    public function thePropertyIsAStringEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();

        $this->thePropertyIsAString($property);

        $actualValue = $this->arrayGet($payload, $property);

        assertSame(
            $actualValue,
            $expectedValue,
            "Asserting the [$property] property in current scope [{$this->scope}] is a string equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a boolean$/
     */
    public function thePropertyIsABoolean($property)
    {
        $payload = $this->getScopePayload();

        assertTrue(
            gettype($this->arrayGet($payload, $property)) == 'boolean',
            "Asserting the [$property] property in current scope [{$this->scope}] is a boolean."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a boolean equalling "([^"]*)"$/
     */
    public function thePropertyIsABooleanEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        if (! in_array($expectedValue, ['true', 'false'])) {
            throw new \InvalidArgumentException("Testing for booleans must be represented by [true] or [false].");
        }

        $this->thePropertyIsABoolean($property);

        assertSame(
            $actualValue,
            $expectedValue == 'true',
            "Asserting the [$property] property in current scope [{$this->scope}] is a boolean equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is a integer equalling "([^"]*)"$/
     */
    public function thePropertyIsAIntegerEqualling($property, $expectedValue)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        $this->thePropertyIsAnInteger($property);

        assertSame(
            $actualValue,
            (int) $expectedValue,
            "Asserting the [$property] property in current scope [{$this->scope}] is an integer equalling [$expectedValue]."
        );
    }

    /**
     * @Given /^the "([^"]*)" property is either:$/
     */
    public function thePropertyIsEither($property, PyStringNode $options)
    {
        $payload = $this->getScopePayload();
        $actualValue = $this->arrayGet($payload, $property);

        $valid = explode("\n", (string) $options);

        assertTrue(
            in_array($actualValue, $valid),
            sprintf(
                "Asserting the [%s] property in current scope [{$this->scope}] is in array of valid options [%s].",
                $property,
                implode(', ', $valid)
            )
        );
    }

    /**
     * @Given /^scope into the first "([^"]*)" property$/
     */
    public function scopeIntoTheFirstProperty($scope)
    {
        $this->scope = "{$scope}.0";
    }

    /**
     * @Given /^scope into the "([^"]*)" property$/
     */
    public function scopeIntoTheProperty($scope)
    {
        $this->scope = $scope;
    }

    /**
     * @Given /^the properties exist:$/
     */
    public function thePropertiesExist(PyStringNode $propertiesString)
    {
        foreach (explode("\n", (string) $propertiesString) as $property) {
            $this->thePropertyExists($property);
        }
    }

    /**
     * @Given /^reset scope$/
     */
    public function resetScope()
    {
        $this->scope = null;
    }

    /**
     * @Transform /^(\d+)$/
     */
    public function castStringToNumber($string)
    {
        return intval($string);
    }

    /**
     * Checks the response exists and returns it.
     *
     * @return  Guzzle\Http\Message\Response
     */
    protected function getResponse()
    {
        if (! $this->response) {
            throw new Exception("You must first make a request to check a response.");
        }

        return $this->response;
    }

    /**
     * Return the response payload from the current response.
     *
     * @return  mixed
     */
    protected function getResponsePayload()
    {
        if (! $this->responsePayload) {
            $json = json_decode($this->getResponse()->getBody(true));

            if (json_last_error() !== JSON_ERROR_NONE) {
                $message = 'Failed to decode JSON body ';

                switch (json_last_error()) {
                    case JSON_ERROR_DEPTH:
                        $message .= '(Maximum stack depth exceeded).';
                        break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $message .= '(Underflow or the modes mismatch).';
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        $message .= '(Unexpected control character found).';
                        break;
                    case JSON_ERROR_SYNTAX:
                        $message .= '(Syntax error, malformed JSON).';
                        break;
                    case JSON_ERROR_UTF8:
                        $message .= '(Malformed UTF-8 characters, possibly incorrectly encoded).';
                        break;
                    default:
                        $message .= '(Unknown error).';
                        break;
                }

                throw new Exception($message);
            }

            $this->responsePayload = $json;
        }

        return $this->responsePayload;
    }

    /**
     * Returns the payload from the current scope within
     * the response.
     *
     * @return mixed
     */
    protected function getScopePayload()
    {
        $payload = $this->getResponsePayload();

        if (! $this->scope) {
            return $payload;
        }

        return $this->arrayGet($payload, $this->scope);
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @copyright   Taylor Otwell
     * @link        http://laravel.com/docs/helpers
     * @param       array   $array
     * @param       string  $key
     * @param       mixed   $default
     * @return      mixed
     */
    protected function arrayGet($array, $key)
    {
        if (is_null($key)) {
            return $array;
        }

        // if (isset($array[$key])) {
        //     return $array[$key];
        // }

        foreach (explode('.', $key) as $segment) {

            if (is_object($array)) {
                if (! isset($array->{$segment})) {
                    return;
                }
                $array = $array->{$segment};

            } elseif (is_array($array)) {
                if (! array_key_exists($segment, $array)) {
                    return;
                }
                $array = $array[$segment];
            }
        }

        return $array;
    }

    /**
     * @When /^I request an access token with valid parameters$/
     */
    public function iRequestAnAccessTokenWithValidParameters()
    {
        $body = file_get_contents($this->appURL.'getValidAccessTokenRequestBody');
        $this->requestPayload['body']=$body;
        $this->response = $this
            ->client
            ->post('/api/iv/v4/token', $this->requestPayload);
    }


    /**
     * @When /^I request a "(valid|invalid)" id for an "(investor|status|certificate|type|verifier|request|investorId|certificate\/html|resend)" "(with|without)" authorization$/
     */
    public function iRequestAIdForAnAuthorization($valid, $type, $authorization)
    {

        if($valid == 'valid' && $authorization == 'with' && ($type == 'investor' || $type == 'status' || $type == 'request')){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidRequestIdForAuthToken'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = $data['requestId'];
        }

        if($valid == 'valid' && $authorization = 'with' && $type == 'investorId'){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidInvestorIdRequest'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = $data['requestId'];
        }

        if($valid == 'valid' && $authorization = 'with' && $type == 'verifier'){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidVerifierRequest'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = $data['requestId'];
        }

        if($valid == 'valid' && $authorization = 'with' && $type == 'type'){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidReportRequest'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = $data['requestId'];
        }

        if($valid == 'valid' && $authorization = 'with' && ($type == 'certificate' || $type == 'certificate/html')){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidCertificateRequest'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = $data['requestId'];
        }


        if($valid == 'invalid' && $authorization == 'without'){
            $this->requestPayload['headers']=['Authorization'=>'Nothing'];
            $id = 35435;
        }

        if($valid == 'invalid' && $authorization == 'with'){
            $data = (array)json_decode(file_get_contents($this->appURL.'getValidRequestIdForAuthToken'));
            $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
            $id = 35343;
        }




        try{
            $this->response = $this->client->get('/api/iv/v4/'.$type.'/'.$id, $this->requestPayload);
        }catch (BadResponseException $e) {

            $response = $e->getResponse();

            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
        $this->requestPayload['body']=[];
        if(isset($this->response->json()['access_token'])){
            $this->accessToken = $this->response->json()['access_token'];
        }

    }

    /**
     * @When /^I create a request for an existing investor with authorization$/
     */
    public function iCreateARequestForAnExistingInvestorWithAuthorization()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getExistingInvestorDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->response = $this->client->get('/api/iv/v4/investor?email='.$data['investorEmail'], $this->requestPayload);
    }

    /**
     * @When /^I get a valid request searching for a verifier$/
     */
    public function iGetAValidRequestSearchingForAVerifier()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getExistingVerifierDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->response = $this->client->get('/api/iv/v4/verifier?email='.$data['verifierEmail'], $this->requestPayload);

    }

    /**
     * @When /^I request all certificates for a key which has at least one certificate$/
     */
    public function iRequestAllCertificatesForAKeyWhichHasAtLeastOneCertificate()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidCertificateRequest'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $this->response = $this->client->get('/api/iv/v4/certificate', $this->requestPayload);
    }

    /**
     * @When /^I request all investor ids with a token that has investor ids$/
     */
    public function iRequestAllInvestorIdsWithATokenThatHasInvestorIds()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidInvestorIdRequest'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $this->response = $this->client->get('/api/iv/v4/investorId', $this->requestPayload);
    }

    /**
     * @When /^I request an existing specific investor id$/
     */
    public function iRequestAnExistingSpecificInvestorId()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidInvestorId'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $this->response = $this->client->get('/api/iv/v4/investorId/search/'.$data['investorId'], $this->requestPayload);

    }


    /**
     * @When /^I request get a valid request seraching for all verifiers$/
     */
    public function iRequestGetAValidRequestSerachingForAllVerifiers()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getExistingVerifierDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->response = $this->client->get('/api/iv/v4/verifier', $this->requestPayload);
    }



    /**
     * @When /^I delete a request with an id I do not own$/
     */
    public function iDeleteARequestWithAnIdIDoNotOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $invalidId = --$data['requestIds'][0];
        $this->callEdgeMethod('delete', 'request/'.$invalidId);

    }

    /**
     * @When /^I delete a request with an id I do own$/
     */
    public function iDeleteARequestWithAnIdIDoOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $id = $data['requestIds'][0];
        $this->callEdgeMethod('delete', 'request/'.$id);
    }

    /**
     * @When /^I delete requests with ids I own$/
     */
    public function iDeleteRequestsWithIdsIOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $ids = $data['requestIds'];
        $this->callEdgeMethod('delete', 'request/'.$ids[0].','.$ids[1]);
    }

    /**
     * @When /^I delete requests with ids I own and do not own$/
     */
    public function iDeleteRequestsWithIdsIOwnAndDoNotOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $ids = $data['requestIds'];
        $this->callEdgeMethod('delete', 'request/'.(--$ids[0]).','.$ids[1]);
    }


    /**
     * @When /^I request to update a request I do not own$/
     */
    public function iRequestToUpdateARequestIDoNotOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $id = --$data['requestIds'][0];
        $this->callEdgeMethod('post', 'request/'.$id);
    }

    /**
     * @When /^I request to update a request on the wrong stage$/
     */
    public function iRequestToUpdateARequestOnTheWrongStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidReportRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'request/'.$data['requestId']);

    }

    /**
     * @When /^I request to update a request I own on the correct stage$/
     */
    public function iRequestToUpdateARequestIOwnOnTheCorrectStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestSentToInvestor'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'request/'.$data['requestId']);
    }

    /**
     * @When /^I request to get the expired status of a request I do not own$/
     */
    public function iRequestToGetTheExpiredStatusOfARequestIDoNotOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $id = --$data['requestIds'][0];
        $this->callEdgeMethod('get', 'expired/'.$id);

    }

    /**
     * @When /^I check the expiration for a request on the correct stage$/
     */
    public function iCheckTheExpirationForARequestOnTheCorrectStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidCertificateRequest'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $this->response = $this->client->get('/api/iv/v4/expired/'.$data['requestId'], $this->requestPayload);
    }

    /**
     * @When /^I check the expiration for a request on the incorrect stage$/
     */
    public function iCheckTheExpirationForARequestOnTheIncorrectStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getUnconfirmedRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $id = $data['requestId'];
        $this->callEdgeMethod('get', 'expired/'.$id);
    }


    /**
     * @When /^I request expired requests with a token that has expired requests$/
     */
    public function iRequestExpiredRequestsWithATokenThatHasExpiredRequests()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getValidCertificateRequest'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $this->response = $this->client->get('/api/iv/v4/expired', $this->requestPayload);
    }


    /**
     * @When /^I request to get the letter of a request I do not own$/
     */
    public function iRequestToGetTheLetterOfARequestIDoNotOwn()
    {

        $data = (array)json_decode(file_get_contents($this->appURL.'getValidCertificateRequest'));
        $this->requestPayload['headers']=['Authorization'=>'Bearer '. $data['token']];
        $id = 100000;
        $this->callEdgeMethod('get', 'letter/'.$id);
    }

    /**
     * @When /^I request to get the letter for a request on the wrong stage$/
     */
    public function iRequestToGetTheLetterForARequestOnTheWrongStage()
    {

        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestSentToInvestor'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('get', 'letter/'.$data['requestId']);
    }

    /**
     * @When /^I request to get a letter with the wrong verification method$/
     */
    public function iRequestToGetALetterWithTheWrongVerificationMethod()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestThirdParty'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('get', 'letter/'.$data['requestId']);
    }

    /**
     * @When /^I request to get a letter with the correct status and verification type$/
     */
    public function iRequestToGetALetterWithTheCorrectStatusAndVerificationType()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestUpload'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('get', 'letter/'.$data['requestId']);
    }

    /**
     * @When /^I request to get all letters$/
     */
    public function iRequestToGetAllLetters()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestUpload'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('get', 'letter');
    }


    /**
     * @When /^I request to get the letter for a request on the wrong stage and one with the correct stage$/
     */
    public function iRequestToGetTheLetterForARequestOnTheWrongStageAndOneWithTheCorrectStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestConfirmedAndNotConfirmed'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $ids = $data['requestIds'];
        $ids = implode(',', $ids);
        $this->callEdgeMethod('get', 'letter/'.$ids);
    }

    /**
     * @When /^I request to get a letter with the wrong verification method and one with the correct verification method$/
     */
    public function iRequestToGetALetterWithTheWrongVerificationMethodAndOneWithTheCorrectVerificationMethod()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestThirdPartyAndUpload'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $ids = $data['requestIds'];
        $ids = implode(',', $ids);
        $this->callEdgeMethod('get', 'letter/'.$ids);
    }

    /**
     * @When /^I check the expiration for a request on the correct stage and one on the incorrect stage$/
     */
    public function iCheckTheExpirationForARequestOnTheCorrectStageAndOneOnTheIncorrectStage()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestConfirmedAndNotConfirmed'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $ids = $data['requestIds'];
        $ids = implode(',', $ids);
        $this->callEdgeMethod('get', 'expired/'.$ids);
    }


    /**
     * @When /^I resend a request to an investor I do not own$/
     */
    public function iResendARequestToAnInvestorIDoNotOwn()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getAllRequestIdsDetails'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $id = --$data['requestIds'][0];
        $this->callEdgeMethod('post', 'resend/'.$id);
    }

    /**
     * @When /^I resend a request to an investor on the wrong status$/
     */
    public function iResendARequestToAnInvestorOnTheWrongStatus()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestUpload'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'resend/'.$data['requestId']);
    }


    /**
     * @When /^I resend a request to an investor on the correct status$/
     */
    public function iResendARequestToAnInvestorOnTheCorrectStatus()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getRequestSentToInvestor'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'resend/'.$data['requestId']);
    }

    /**
     * @When /^I request to move a request I do not own to the next step in the sandbox environment$/
     */
    public function iRequestToMoveARequestIDoNotOwnToTheNextStepInTheSandboxEnvironment()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/nextStep/534634634634');
    }


    /**
     * @When /^I request to move a request to the next step in the production environment$/
     */
    public function iRequestToMoveARequestToTheNextStepInTheProductionEnvironment()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getProductionRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/nextStep/'.$data['requestId']);
    }



    /**
     * @When /^I request to move a request I own in the sandbox environment to an invalid step$/
     */
    public function iRequestToMoveARequestIOwnInTheSandboxEnvironmentToAnInvalidStep()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getCompleteSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/nextStep/'.$data['requestId']);
    }


    /**
     * @When /^I request to move a request I own in the sandbox environment to a valid step$/
     */
    public function iRequestToMoveARequestIOwnInTheSandboxEnvironmentToAValidStep()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getIncompleteSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/nextStep/'.$data['requestId']);
    }


    /**
     * @When /^I request to move a request I do not own to the previous step in the sandbox environment$/
     */
    public function iRequestToMoveARequestIDoNotOwnToThePreviousStepInTheSandboxEnvironment()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/previousStep/534634634634');
    }



    /**
     * @When /^I request to move a request to the previous step in the production environment$/
     */
    public function iRequestToMoveARequestToThePreviousStepInTheProductionEnvironment()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getProductionRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/previousStep/'.$data['requestId']);
    }

    /**
     * @When /^I request to move a request I own in the sandbox environment to an invalid previous step$/
     */
    public function iRequestToMoveARequestIOwnInTheSandboxEnvironmentToAnInvalidPreviousStep()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getStartedSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/previousStep/'.$data['requestId']);
    }



    /**
     * @When /^I request to move a request I own in the sandbox environment to a valid previous step$/
     */
    public function iRequestToMoveARequestIOwnInTheSandboxEnvironmentToAValidPreviousStep()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getNotStartedSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/previousStep/'.$data['requestId']);
    }

    /**
     * @When /^I request to purge with a non sandbox key$/
     */
    public function iRequestToPurgeWithANonSandboxKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getProductionRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('delete', 'testing/purge');
    }

    /**
     * @When /^I request to purge a sandbox key$/
     */
    public function iRequestToPurgeASandboxKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('delete', 'testing/purge');

    }


    /**
     * @When /^I request to seed with a non sandbox key$/
     */
    public function iRequestToSeedWithANonSandboxKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getProductionRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/seed');
    }

    /**
     * @When /^I request to seed with a sandbox key$/
     */
    public function iRequestToSeedWithASandboxKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/seed');
    }


    /**
     * @When /^I request to seed more than (\d+) requests$/
     */
    public function iRequestToSeedMoreThanRequests($arg1)
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'getSandboxRequest'));
        $this->requestPayload['headers'] = ["Authorization"=>"Bearer ".$data['token']];
        $this->callEdgeMethod('post', 'testing/seed?count=1000');
    }



    /**
     * @Given /^I seed the database$/
     */
    public function iSeedTheDatabase()
    {
        file_get_contents($this->appURL.'seedDatabase');
    }

    /**
     * @When /^I send a request using v3 of the API$/
     */
    public function iSendARequestUsingV3OfTheApi()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomCredentials'));
        $faker = \Faker\Factory::create();
        $this->callMethodWithVersion('v3', 'get', 'sendRequest?'.http_build_query([
                'key'=>$data['private'],
                'fName'=>$faker->firstName,
                'lName'=>$faker->lastName,
                'phone'=>$faker->phoneNumber,
                'email'=>$faker->email
            ]));
    }


    /**
     * @When /^I send a request using v(\d+) of the API without a first or last name$/
     */
    public function iSendARequestUsingVOfTheApiWithoutAFirstOrLastName($arg1)
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomCredentials'));
        $faker = \Faker\Factory::create();
        $this->callMethodWithVersion('v3', 'get', 'sendRequest?'.http_build_query([
                'key'=>$data['private'],
                'phone'=>$faker->phoneNumber,
                'email'=>$faker->email
            ]));
    }


    /**
     * @When /^I send a request using v(\d+) of the API without an email$/
     */
    public function iSendARequestUsingVOfTheApiWithoutAnEmail($arg1)
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomCredentials'));
        $faker = \Faker\Factory::create();
        $this->callMethodWithVersion('v3', 'get', 'sendRequest?'.http_build_query([
                'key'=>$data['private'],
                'fName'=>$faker->firstName,
                'lName'=>$faker->lastName,
                'phone'=>$faker->phoneNumber
            ]));
    }

    /**
     * @When /^I send a request using v(\d+) of the API without a phone number$/
     */
    public function iSendARequestUsingVOfTheApiWithoutAPhoneNumber($arg1)
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomCredentials'));
        $faker = \Faker\Factory::create();
        $this->callMethodWithVersion('v3', 'get', 'sendRequest?'.http_build_query([
                'key'=>$data['private'],
                'fName'=>$faker->firstName,
                'lName'=>$faker->lastName,
                'email'=>$faker->email
            ]));
    }


    /**
     * @When /^I get a request with a valid id and key$/
     */
    public function iGetARequestWithAValidIdAndKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomRequestWithCredentials'));
        $this->callMethodWithVersion('v3', 'get', 'getRequest?'.http_build_query([
                'key'=>$data['private'],
                'id'=>$data['id']
            ]));
    }


    /**
     * @When /^I get a request with an invalid id and key$/
     */
    public function iGetARequestWithAnInvalidIdAndKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomRequestWithCredentials'));
        $this->callMethodWithVersion('v3', 'get', 'getRequest?'.http_build_query([
                'key'=>$data['private'],
                'id'=>10000000
            ]));
    }


    /**
     * @When /^I get a request with an invalid key$/
     */
    public function iGetARequestWithAnInvalidKey()
    {
        $data = (array)json_decode(file_get_contents($this->appURL.'randomRequestWithCredentials'));
        $this->callMethodWithVersion('v3', 'get', 'getRequest?'.http_build_query([
                'key'=>$data['public'],
                'id'=>$data['id']
            ]));
    }




    /**
     * @param $method
     * @param $endpoint
     * @internal param $ids
     * @internal param $request
     */
    public function callEdgeMethod($method, $endpoint)
    {
        try {
            $this->response = $this->client->$method('api/iv/v4/'.$endpoint, $this->requestPayload);
        } catch (BadResponseException $e) {

            $response = $e->getResponse();

            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
        $this->requestPayload['body'] = [];
        if (isset($this->response->json()['access_token'])) {
            $this->accessToken = $this->response->json()['access_token'];
        }
    }


    /**
     * @param $version
     * @param $method
     * @param $endpoint
     * @internal param $ids
     * @internal param $request
     */
    public function callMethodWithVersion($version, $method, $endpoint)
    {
        try {
            $this->response = $this->client->$method('api/iv/'.$version.'/'.$endpoint, $this->requestPayload);
        } catch (BadResponseException $e) {

            $response = $e->getResponse();

            // Sometimes the request will fail, at which point we have
            // no response at all. Let Guzzle give an error here, it's
            // pretty self-explanatory.
            if ($response === null) {
                throw $e;
            }

            $this->response = $e->getResponse();
        }
        $this->requestPayload['body'] = [];
        if (isset($this->response->json()['access_token'])) {
            $this->accessToken = $this->response->json()['access_token'];
        }
    }





}