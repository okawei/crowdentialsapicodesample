Feature: Testing Next Step


  Scenario: Attempt to move a request to the next step without authorization
    When I request "POST /api/iv/v4/testing/nextStep/3433534" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Attempt to move a request I do not own to the next step with authorization
    When I request to move a request I do not own to the next step in the sandbox environment
    Then I get a "401" response
    And the "error" property exists

  Scenario: Attempt to move a request I do own with a non sandbox key
    When I request to move a request to the next step in the production environment
    Then I get a "400" response
    And the "error" property exists

  Scenario: Attempt to move a request I own in the sandbox environment to an invalid step
    When I request to move a request I own in the sandbox environment to an invalid step
    Then I get a "403" response
    And the "error" property exists

  Scenario: Move a request I own in the sandbox environment to a valid step
    When I request to move a request I own in the sandbox environment to a valid step
    Then I get a "201" response
    And the "data" property exists