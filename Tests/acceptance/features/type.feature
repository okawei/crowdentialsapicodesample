Feature: Request Type


  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "type" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "type" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Finding specific type details
    When I request a "valid" id for an "type" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    typeCode
    typeDetails
      """
    And the "id" property is an integer

  Scenario: Finding all type details
    When I request "GET /api/iv/v4/type" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    typeCode
    typeDetails
      """
    And the "id" property is an integer

  Scenario: Searching a non-existent type
    When I request "GET /api/iv/v4/type?code=33" "with" authorization
    Then I get a "200" response
    And the "data" property contains 0 items

  Scenario: Searching an existent type
    When I request "GET /api/iv/v4/type?code=1,2,3,4,5" "with" authorization
    Then I get a "200" response
    And the "data" property contains at least 1 items