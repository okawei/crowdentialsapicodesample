Feature: Testing Seed



  Scenario: Attempt to move a request to seed without authorization
    When I request "POST /api/iv/v4/testing/seed" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Attempt to seed with a non sandbox key
    When I request to seed with a non sandbox key
    Then I get a "400" response
    And the "error" property exists


  Scenario: Attempt to seed greater than 10 requests
    When I request to seed more than 10 requests
    Then I get a "400" response
    And the "error" property exists

  Scenario: Seed a sandbox key
    When I request to seed with a sandbox key
    Then I get a "200" response
    And the "success" property exists