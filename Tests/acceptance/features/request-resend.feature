Feature: Request Resend

  Scenario: Resend a request to an investor with no authorization
    When I request "POST /api/iv/v4/resend/1/" "without" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Resend a request to an investor I do not own
    When I resend a request to an investor I do not own
    Then I get a "401" response
    And the "error" property exists

  Scenario: Resend a request to an investor on the wrong status
    When I resend a request to an investor on the wrong status
    Then I get a "403" response
    And the "error" property exists
    
  Scenario: Resend a request to an investor I do own on the correct status
    When I resend a request to an investor on the correct status
    Then I get a "200" response
    And the "success" property exists
    
