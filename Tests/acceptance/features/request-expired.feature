Feature: Request Expired

  Scenario: Check the expiration of a request with no authorization
    When I request "GET /api/iv/v4/expired/1" "without" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Check the expiration of a request I do not own
    When I request to get the expired status of a request I do not own
    Then I get a "401" response
    And the "error" property exists

  Scenario: Get all expired requests
    When I request expired requests with a token that has expired requests
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    expired
    investor
    """
    And the "id" property is an integer


  Scenario: Check expiration on a request on the correct stage
    When I check the expiration for a request on the correct stage
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    expired
    investor
    """
    And the "id" property is an integer

  Scenario: Check expiration on a request on the correct stage and one on the incorrect stage
    When I check the expiration for a request on the correct stage and one on the incorrect stage
    Then I get a "403" response
    And the "error" property exists


  Scenario: Check expiration on a request on the incorrect stage
    When I check the expiration for a request on the incorrect stage
    Then I get a "403" response
    And the "error" property exists
    And scope into the "error" property
    And the "expectedStatus" property exists
    And the "actualStatus" property exists