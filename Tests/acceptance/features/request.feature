Feature: Request General

  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "request" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "request" "without" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Getting all requests
    When I request "GET /api/iv/v4/request" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      id
      investor
      status
      verifier
      """
    And the "id" property is an integer

  Scenario: Get request with id
    When I request a "valid" id for an "request" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      id
      investor
      status
      verifier
      """
    And the "id" property is an integer
