Feature: V3 api calls

  Scenario: Send a request without the first or last name
    When I send a request using v3 of the API without a first or last name
    Then I get a "200" response
    And the "errors" property exists


  Scenario: Send a request without the first or last name
    When I send a request using v3 of the API without an email
    Then I get a "200" response
    And the "errors" property exists

  Scenario: Send a request without the first or last name
    When I send a request using v3 of the API without a phone number
    Then I get a "200" response
    And the "errors" property exists

  Scenario: Send A valid request
    When I send a request using v3 of the API
    Then I get a "200" response
    And the "requestId" property exists
    And the "timestamp" property exists


  Scenario: Get a valid request Id
    When I get a request with a valid id and key
    Then I get a "200" response
    And the "state" property exists

  Scenario: Get an invalid request Id
    When I get a request with an invalid id and key
    Then I get a "200" response
    And the "response" property exists

  Scenario: Get an invalid api key
    When I get a request with an invalid key
    Then I get a "200" response
    And the "response" property exists


