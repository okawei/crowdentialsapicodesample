Feature: Request Status


  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "status" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "status" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Finding specific investor details
    When I request a "valid" id for an "status" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    status
    statusCode
    statusDetails
      """
    And the "id" property is an integer

  Scenario: Finding all type details
    When I request "GET /api/iv/v4/status" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    status
    statusCode
    statusDetails
      """
    And the "id" property is an integer

  Scenario: Searching a non-existent status
    When I request "GET /api/iv/v4/status?code=33" "with" authorization
    Then I get a "200" response
    And the "data" property contains 0 items

  Scenario: Searching an existent status
    When I request "GET /api/iv/v4/status?code=1,2,3,4" "with" authorization
    Then I get a "200" response
    And the "data" property contains at least 1 items