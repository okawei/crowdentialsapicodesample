Feature: Request Investor Details

  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "investor" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "investor" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Finding specific investor details
    When I request a "valid" id for an "investor" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
      And the properties exist:
        """
      id
      name
      email
      phone
      type
      createdAt
      updatedAt
      """
    And the "id" property is an integer


  Scenario: Finding all investor details
    When I request "GET /api/iv/v4/investor" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    name
    email
    phone
    type
    createdAt
    updatedAt
    """
    And the "id" property is an integer


  Scenario: Searching a non-existent investor
    When I request "GET /api/iv/v4/investor?name=pwhgapoiwghapwoeghapwogehaoweghiapowighapew9g89wg&phone=3845389438&email=not@real.email" "with" authorization
    Then I get a "200" response
    And the "data" property contains 0 items


  Scenario: Searching for an existing investor
    When I create a request for an existing investor with authorization
    Then I get a "200" response
    And the "data" property contains at least 1 items