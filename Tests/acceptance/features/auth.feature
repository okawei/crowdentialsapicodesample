Feature: Authentication


  Scenario: Request an access token with invalid parameters
#    This has to be here as the first step run to ensure that we are running on a clean db
    And I seed the database
    Given I have the payload:
    """
    {"grant_type":"client_credentials", "client_id":"not valid", "client_secret":"you're done son"}
    """
    When I request "POST /api/iv/v4/token" "without" authorization
    Then I get a "400" response
    And the properties exist:
    """
    error
    error_description
    """

  Scenario: Request an access token with valid parameters
    When I request an access token with valid parameters
    Then I get a "200" response
    And the properties exist:
    """
    access_token
    expires_in
    token_type
    scope
    """