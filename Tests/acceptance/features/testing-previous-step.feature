Feature: Testing Previous Step


  Scenario: Attempt to move a request to the previous step without authorization
    When I request "POST /api/iv/v4/testing/previousStep/3433534" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Attempt to move a request I do not own to the previous step with authorization
    When I request to move a request I do not own to the previous step in the sandbox environment
    Then I get a "401" response
    And the "error" property exists

  Scenario: Attempt to move a request I do own with a non sandbox key
    When I request to move a request to the previous step in the production environment
    Then I get a "400" response
    And the "error" property exists

  Scenario: Attempt to move a request I own in the sandbox environment to an invalid previous step
    When I request to move a request I own in the sandbox environment to an invalid previous step
    Then I get a "403" response
    And the "error" property exists

  Scenario: Move a request I own in the sandbox environment to a valid previous step
    When I request to move a request I own in the sandbox environment to a valid previous step
    Then I get a "201" response
    And the "data" property exists