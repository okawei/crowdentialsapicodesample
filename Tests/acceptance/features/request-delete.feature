Feature: Request Delete

  Scenario: Delete a request with no authorization
    When I request "DELETE /api/iv/v4/request/1" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Delete a request with authorization and no id specified
    When I request "DELETE /api/iv/v4/request/" "with" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Delete a request with an id I do not own
    When I delete a request with an id I do not own
    Then I get a "401" response
    And the "error" property exists


  Scenario: Delete a request with an id I do own
    When I delete a request with an id I do own
    Then I get a "200" response
    And the "success" property exists

  Scenario: Delete multiple requests with ids I own
    When I delete requests with ids I own
    Then I get a "200" response
    And the "success" property exists

  Scenario: Mixed owned and unowned ids delete request
    When I delete requests with ids I own and do not own
    Then I get a "401" response
    And the "error" property exists