Feature: Request Update

  Scenario: Update a request with no authorization
    When I request "POST /api/iv/v4/request/1" "without" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Update a request I do not own
    When I request to update a request I do not own
    Then I get a "403" response
    And the "error" property exists

  Scenario: Update a request on the wrong stage
    When I request to update a request on the wrong stage
    Then I get a "403" response
    And the "error" property exists
    And scope into the "error" property
    And the "expectedStatus" property exists
    And the "actualStatus" property exists

  Scenario: Update a request I own on the correct stage with no data
    Given I have the payload:
    """
    {}
    """
    When I request to update a request I own on the correct stage
    Then I get a "400" response
    And the "error" property exists

  Scenario: Update a request I own on the correct stage
    Given I have the payload:
    """
    {"jibberJabber":"not real"}
    """
    When I request to update a request I own on the correct stage
    Then I get a "400" response
    And the "error" property exists

  Scenario: Update a request I own on the correct stage
    Given I have the payload:
    """
    {"name":"Billy Joe Smith"}
    """
    When I request to update a request I own on the correct stage
    Then I get a "200" response
    And the "success" property exists

