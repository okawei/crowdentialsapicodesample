Feature: Request Certificate

  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "certificate" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "certificate" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Finding specific investor details
    When I request a "valid" id for an "certificate" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      state
      investorName
      verifiedDate
      prettyVerifiedDate
      legal
      disclaimer
      isUpload
      certificateId
      accredited
      id
      verifier
      """
    And the "id" property is an integer
    And the "verifier" property is an object


  Scenario: Finding all investor details
    When I request all certificates for a key which has at least one certificate
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      state
      investorName
      verifiedDate
      prettyVerifiedDate
      legal
      disclaimer
      isUpload
      certificateId
      accredited
      id
      verifier
      """
    And the "id" property is an integer
    And the "verifier" property is an object

  Scenario: Get certificate HTML
    When I request a "valid" id for an "certificate/html" "with" authorization
    Then I get a "200" response
    And scope into the "data" property
    And the "html" property exists