Feature: Request Verifier


  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "verifier" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "verifier" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Searching for an existing verifier
    When I request a "valid" id for an "verifier" "with" authorization
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    individualName
    entityName
    email
    phone
    zip
    state
    type
    secOrCRD
    licenseNumber
    createdAt
    updatedAt
    """
    And the "id" property is an integer

  Scenario: Getting all verifiers
    When I request get a valid request seraching for all verifiers
    Then I get a "200" response
    And the "data" property contains at least 1 items

  Scenario: Searching for a valid verifier
    When I get a valid request searching for a verifier
    Then I get a "200" response
    And the "data" property contains at least 1 items

  Scenario: Searching for an invalid verifier
    When I request "GET /api/iv/v4/verifier?name=Billy Bob Barker Esquire The Fourth" "with" authorization
    Then I get a "200" response
    And the "data" property contains 0 items


