Feature: Testing Purge



  Scenario: Attempt to move a request to the previous step without authorization
    When I request "DELETE /api/iv/v4/testing/purge" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Attempt to purge with a non sandbox key
    When I request to purge with a non sandbox key
    Then I get a "400" response
    And the "error" property exists

  Scenario: Purge a sandbox key
    When I request to purge a sandbox key
    Then I get a "200" response
    And the "success" property exists