Feature: Request Post

  Scenario: Post a request with no authorization
    When I request "POST /api/iv/v4/request/" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Post a request with missing parameters
    Given I have the payload:
    """
    {"jibberJabber":"not real"}
    """
    When I request "POST api/iv/v4/request" "with" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Post a request with invalid parameters
    Given I have the payload:
    """
    {"name":"Name", "email":"email", "phone":""}
    """
    When I request "POST api/iv/v4/request" "with" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Post a request with valid parameters
    Given I have the payload:
    """
    {"name":"Name", "email":"heckel.max+fakeitMK@gmail.com", "phone":"awegaweg", "investorType":"natural", "investorId":"fakeId"}
    """
    When I request "POST api/iv/v4/request" "with" authorization
    Then I get a "201" response
    And the "success" property exists
