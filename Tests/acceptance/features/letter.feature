Feature: Request Letter


  Scenario: Check the letter of a request with no authorization
    When I request "GET /api/iv/v4/letter/1/" "without" authorization
    Then I get a "400" response
    And the "error" property exists

  Scenario: Check the letter of a request I do not own
    When I request to get the letter of a request I do not own
    Then I get a "401" response
    And the "error" property exists

  Scenario: Get a letter for a request on the wrong stage
    When I request to get the letter for a request on the wrong stage
    Then I get a "403" response
    And the "error" property exists

  Scenario: Get a letter for a request on the wrong stage and one with the correct stage
    When I request to get the letter for a request on the wrong stage and one with the correct stage
    Then I get a "403" response
    And the "error" property exists

  Scenario: Get a letter for a request with the wrong verification method
    When I request to get a letter with the wrong verification method
    Then I get a "400" response
    And the "error" property exists

  Scenario: Get a letter for a request with the wrong verification method and one with the correct verification method
    When I request to get a letter with the wrong verification method and one with the correct verification method
    Then I get a "400" response
    And the "error" property exists

  Scenario: Get a letter for a request on the correct status and the correct verification type
    When I request to get a letter with the correct status and verification type
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    publicURL
    privateURL
    """

  Scenario: Get all letters
    When I request to get all letters
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
    """
    id
    publicURL
    privateURL
    """

