Feature: Request Investor Id

  Scenario: Searching for a non-existent request id with authorization
    When I request a "invalid" id for an "investorId" "with" authorization
    Then I get a "401" response
    And the "error" property exists

  Scenario: Searching for a non-existent request id without authorization
    When I request a "invalid" id for an "investorId" "without" authorization
    Then I get a "400" response
    And the "error" property exists


  Scenario: Get all investor ids
    When I request all investor ids with a token that has investor ids
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      id
      investorId
      """
    And the "id" property is an integer

  Scenario: Get specific investor Id
    When I request an existing specific investor id
    Then I get a "200" response
    And scope into the first "data" property
    And the properties exist:
      """
      id
      investor
      status
      verifier
      """
    And the "id" property is an integer

  Scenario: Get specific investor Id
    When I request "GET /api/iv/v4/investorId/search/oaweihg9a0we8gh9a038h02ah3" "with" authorization
    Then I get a "200" response
    And the "data" property contains 0 items