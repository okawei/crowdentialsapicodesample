<?php



namespace Models\iv;


class Document extends \Eloquent{


    protected $table = 'documents';



    protected $fillable = ['reportId','mime','key','type'];



    public function report(){
        return $this->belongsTo('NaturalReport', 'reportId');
    }
}