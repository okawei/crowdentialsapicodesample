<?php

namespace Models\iv;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class VeritaxReport extends \Eloquent{

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'veritaxReports';
    protected $fillable = ['requestId', 'secureId', 'orderNumber', 'status'];


    public function request(){
        return $this->hasOne('Models\iv\Request', 'id', 'requestId');
    }

    public function report(){
        return $this->hasOne('Models\iv\Report', 'reportId', 'id');
    }

    public function getReport(){
        return Report::where('reportId', $this->id)->where('type', 6)->first();
    }

}
