<?php



namespace Models\iv;


use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Report extends \Eloquent{

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $table = 'reports';



    protected $fillable = ['requestId','type','reportId'];



    public function request(){
        return $this->belongsTo('Models\iv\Request', 'requestId', 'id');
    }

    public function getReport(){
        if($this->type == 1){
            return NaturalReport::find($this->reportId);
        }

        if($this->type == 2){
            return TrustReport::find($this->reportId);
        }

        if($this->type == 3){
            return CorporationReport::find($this->reportId);
        }
        if($this->type == 4){
            return LLCReport::find($this->reportId);
        }
        if($this->type == 5){
            return PartnershipReport::find($this->reportId);
        }
        if($this->type == 6){
            return VeritaxReport::find($this->reportId);
        }
    }
    public function verification(){
        return $this->hasOne('Models\iv\Verification', 'reportId');
    }


    public function getDocuments(){
        return Document::where('reportId', $this->id)->get();
    }
    public function getTypeText(){
        if($this->type == 1){
            return "Natural Person";
        }
        if($this->type == 2){
            return "Trust";
        }
        if($this->type == 3){
            return "Corporation";
        }
        if($this->type == 4){
            return "LLC";
        }
        if($this->type == 5){
            return "Partnership";
        }
        if($this->type == 6){
            return "Natural Person";
        }
    }

    public function getCSEmails(){
        return csResentEmail::where('requestId', $this->requestId)->where('sentTo', '2');
    }



}