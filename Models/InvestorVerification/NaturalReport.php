<?php



namespace Models\iv;

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Models\iv\csResentEmail;

class NaturalReport extends \Eloquent{

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $table = 'naturalReports';

    public $sandboxFlag = false;

    protected $fillable = ['requestId', 'thirdParty', 'thirdPartyType', 'privatePlacements', 'acaMember', 'selfCertifyAI', 'verifierName', 'verificationType', 'verifierType', 'verifierEmail', 'verifierPhone', 'investorName', 'taxReturnYears', 'consentEffectiveFor', 'signatureName', 'signature', 'secureId', 'verifyBasedOn', 'hasThirdParty', 'thirdPartyHasLiabilities', 'thirdPartyCanGainAccessToLiabilities', 'thirdPartyHasAssets', 'hasFullLiabilities', 'selfCertifyQP', 'ownAccount', 'outstandingBusiness', 'outstandingBusinessAmount'];

    public function request(){

        return $this->hasOne('Models\iv\Request', 'id', 'requestId');
    }

    public function report(){
        return $this->hasOne('Models\iv\Report', 'reportId', 'id');
    }

    public function getReport(){
        return Report::where('reportId', $this->id)->where('type', 1)->first();
    }

    public function getCSEmails(){
        return csResentEmail::where('requestId', $this->request->id)->where('sentTo', '2');
    }


    public function documents(){

        return $this->hasMany('Models\iv\Document', 'reportId');
    }

    public function getDocuments(){
        return Document::where('reportId', $this->report->id)->get();
    }

    public function upload(){
        return $this->hasOne('Models\iv\NaturalUpload', 'reportId');
    }



//
//    public function save(array $options = []){
//        if($this->request->apiId != 0){
//            $key = $this->request->key;
//            if($key->type == 1){
//                $this->connection = 'iv_sandbox';
//                parent::save($options);
//                return;
//            }
//        }
//        parent::save($options);
//    }
}