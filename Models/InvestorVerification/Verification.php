<?php



namespace Models\iv;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Verification extends \Eloquent{

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $table = 'verifications';

    protected $fillable = [ 'income', 'assets', 'netWorth', 'aiNone', 'qpAnswer', 'entityName', 'stateOfIncorporation', 'zip', 'licenseNumber', 'phoneNumber', 'signatureName', 'signature', 'reportId', 'secureId', 'secOrCRD', 'confirmed', 'requestId', 'trust', 'corporation', 'llc', 'partnership'];

    public $sandboxFlag = false;

    public function report(){
        return $this->hasOne('Models\iv\Report', 'id', 'reportId');
    }
    public function request(){
        return $this->hasOne('Models\iv\Request', 'id', 'requestId');
    }

}