<?php

namespace Models\iv;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class TrustReport extends \Eloquent{

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
    protected $table = 'trustReports';
    protected $fillable = ['requestId', 'secureId', 'authorizedToAct', 'notFormedForAcquiring', 'sophisticatedPerson', 'tosPPCheck'];


    public function request(){
        return $this->hasOne('Models\iv\Request', 'id', 'requestId');
    }

    public function report(){
        return $this->hasOne('Models\iv\Report', 'reportId', 'id');
    }

    public function getReport(){
        return Report::where('reportId', $this->id)->where('type', 2)->first();
    }

}
