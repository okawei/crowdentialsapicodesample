<?php



namespace Models\iv;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Request extends \Eloquent{

    use SoftDeletingTrait;

    protected $dates = ['deleted_at'];

    protected $table = 'requests';


    protected $fillable = ['roundName', 'issuerId', 'investmentAmount', 'previousInvestment', 'doubts', 'howTheyMet', 'researchedBackground', 'investorName', 'investorPhone', 'investorEmail', 'personalNote', 'lastContacted', 'secureId', 'linkAPI', 'unlocked', 'customerService', 'qpFlag', 'apiId', 'investorType'];

    public $testMode = false;


    public static $rules = [
        'roundName'             => 'required|between:1,16',
        'issuerId'              => 'required',
        'investmentAmount'      => 'required',
        'doubts'                => 'required',
        'previousInvestment'    => 'required',
        'howTheyMet'            => 'required',
        'researchedBackground'  => 'required',
        'investorName'          => 'required|between:1,16',
        'investorPhone'         => 'required|min:9',
        'investorEmail'         => 'required|email',
        'secureId'              => 'required',

    ];

    public function report(){

        return $this->hasOne('Models\iv\Report', 'requestId');
    }

    public function verification(){

        return $this->hasOne('Models\iv\Verification', 'requestId');
    }



    public function getReport(){
        if($this->report != null){
            return $this->report->getReport();
        }
        return null;
    }

    public function getVerification(){
        if($this->report != null){
            return $this->verification;
        }
        return null;
    }

    public function getConfirmed(){
        if($this->verification != null){
            return $this->verification->confirmed;
        }
        return null;
    }

    public function issuer(){
        return $this->hasOne('User', 'id', 'issuerId');
    }

    public function clientOverride(){
        return $this->hasOne('Models\iv\ClientOverride', 'requestId');
    }

    public function key(){

        return $this->hasOne('APIKey', 'id', 'apiId');;
    }


    public function csNotes(){
        return $this->hasMany('Models\iv\csNote', 'requestId', 'id');
    }

    public function csResentEmails(){
        return $this->hasMany('Models\iv\csResentEmail', 'requestId', 'id');
    }

    public function save(array $options = []){
        if($this->apiId != 0){
            $key = $this->key;
            if($key->type == 1){
                $this->connection = 'iv_sandbox';
                parent::save($options);
                return;
            }
        }
        parent::save($options);
    }


    public function getCustomerService(){
        return $this->customerService;
    }

    public function getStatus(){
        $addArray = [];
        $addArray['status'] = "Sent to Investor";
        $addArray['lastAction'] = date('m/d/Y', strtotime($this->created_at));
        $addArray['code']=1;
        $addArray['details']="The accreditation request has been sent to the investor. The investor has not yet completed the request.";
        if ($this->report != null) {

            $addArray['status'] = "Sent to Verifier (Third Party)";
            if($this->getReport()->verificationType == 'upload' || $this->report->type != 1){
                $addArray['status'] = "Sent to Verifier (Crowdentials' Attorney)";
            }
            $addArray['lastAction'] = date('m/d/Y', strtotime($this->getReport()->created_at));
            $addArray['code']=2;
            $addArray['details']="The investor has entered his or her verifier's information.  The verifier has been notified of the request.";
            if ($this->verification != null) {
                $addArray['status'] = "Verification Complete";
                $addArray['lastAction'] = date('m/d/Y', strtotime($this->verification->created_at));
                $addArray['code']=3;
                $addArray['details']="The verifier has completed verifying the investors accredited status.  Crowdentials is currently confirming the verifier's identity.";
                if ($this->verification->confirmed) {
                    $addArray['status'] = "Confirmed (Third Party)";
                    if($this->getReport()->verificationType == 'upload' || $this->report->type != 1){
                        $addArray['status'] = "Confirmed (Crowdentials' Attorney)";
                    }

                    $addArray['lastAction'] = date('m/d/Y', strtotime($this->verification->updated_at));
                    $addArray['code']=4;
                    $addArray['details']="The accreditation certificate is completed and is ready to view.";
                }
            }
        }
        return $addArray;
    }


    public function isComplete(){
            return $this->verification->confirmed;
    }

    public function investorId(){
        return $this->hasOne('Models\iv\RequestInvestorId', 'requestId');
    }


    public function howTheyMetText(){
        if($this->howTheyMet == 0){
            return 'Family or Friend';
        } else if($this->howTheyMet == 1){
            return 'Previous Relationship';
        } else if($this->howTheyMet == 2){
            return 'Referral';
        } else if($this->howTheyMet == 3){
            return 'Private Presentation';
        } else if($this->howTheyMet == 4){
            return 'Public Presentation';
        } else{
            return 'Other Form of General Solicitation';
        }
    }
    public function delete()
    {


        if($this->issuer != null){
            if($this->issuer->details->docUploadFlag && $this->issuer->details->billingPeriod == 2){

                if(Carbon::createFromTimestamp(strtotime($this->created_at))->diffInDays() <= 2 && $this->verification == null && $this->linkAPI){

                    $this->issuer->details->amountOwed = $this->issuer->details->amountOwed-10;
                    $this->issuer->details->save();
                }
            }
        }

        if($this->key != null){
            if($this->key->user->details->sass && $this->key->user->details->activated && $this->key->type == 2){
                if($this->report == null){
                    $this->key->user->apiBilling->amountOwed = $this->key->user->apiBilling->amountOwed - $this->key->user->apiBilling->transactionAmount;
                    $this->key->user->apiBilling->save();
                }
            }
        }

        if($this->report != null){

            $this->getReport()->delete();
            if($this->verification != null){
                $this->verification()->delete();
            }

            $this->report()->delete();
        }


        return parent::delete();
    }

    public function getClient(){
        if($this->apiId != null){
            return $this->key->client;
        }
        if($this->issuer != null){
            return $this->issuer->details->companyName;
        }

    }

    public function getVerifier(){
        if($this->report == null){
            return 'N/A';
        }

        if($this->report->type != 1){
            return 'Jim Rench';
        }

        if($this->getReport()->verificationType != 'upload'){
            return $this->getReport()->verifierName;
        }

        return 'Jim Rench';
    }
}