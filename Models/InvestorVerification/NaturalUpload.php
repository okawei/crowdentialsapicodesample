<?php



namespace Models\iv;


use Illuminate\Database\Eloquent\SoftDeletingTrait;

class NaturalUpload extends \Eloquent{

    use SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    protected $table = 'naturalUploads';



    protected $fillable = ['reportID','incomeType','liabilitiesAmount'];



    public function report(){
        return $this->belongsTo('NaturalReport', 'reportId');
    }
}