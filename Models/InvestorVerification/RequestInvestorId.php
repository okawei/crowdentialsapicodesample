<?php


namespace Models\iv;



class RequestInvestorId extends \Eloquent{



    protected $table = 'requestInvestorIds';


    protected $fillable = ['requestId', 'investorId'];


    public function request(){
        return $this->belongsTo('Models\iv\Request', 'requestId');
    }
}