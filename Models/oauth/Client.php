<?php

namespace Models\oauth;



class Client extends \Eloquent{

    public $timestamps = false;

    protected $connection = 'oauth';

    protected $table = 'oauth_clients';

    protected $fillable = ['client_id', 'client_secret', 'user_id'];


}