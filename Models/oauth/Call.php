<?php

namespace Models\oauth;



class Call extends \Eloquent{

    public $maxCalls = 15;

    protected $connection = 'oauth';

    protected $table = 'calls';

    protected $fillable = ['uri', 'method', 'keyId'];

}